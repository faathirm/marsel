<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
});
 */
//


//AJAX
Route::post('/ajax/getdatabarang','App\Http\Controllers\AjaxController@getDataBarang')->name('ajax_getdatabarang');
Route::post('/ajax/getdatabarangmasuk','App\Http\Controllers\AjaxController@getDataBarangMasuk')->name('ajax_getdatabarangmasuk');
Route::post('/ajax/getpembeli','App\Http\Controllers\AjaxController@getPembeli')->name('ajax_getpembeli');
Route::post('/ajax/getdatapembayaran','App\Http\Controllers\AjaxController@getDataPembayaran')->name('ajax_getdatapembayaran');
Route::post('/ajax/getdatapembeli','App\Http\Controllers\AjaxController@getDataPembeli')->name('ajax_getdatapembeli');
Route::post('/ajax/getdatapengembalian','App\Http\Controllers\AjaxController@getDataPengembalian')->name('ajax_getdatapengembalian');

//login
Route::get('/login',[\App\Http\Controllers\UserController::class,'login'])->name('login');
Route::post('/login',[\App\Http\Controllers\UserController::class,'loginPost'])->name('loginPost');

Route::group(['middleware'=>['customAuth']], function(){
    Route::get('/', 'App\Http\Controllers\InventarisController@table_datatable_basic');
    Route::get('/dashboard',[\App\Http\Controllers\Controller::class, 'redirect503']);
    Route::get('/inventaris','App\Http\Controllers\InventarisController@table_datatable_basic');

    Route::get('/inventaris/masuk','App\Http\Controllers\InventarisController@table_datatable_basic')->name('inventarisMasuk');
    Route::get('/inventaris/masuk/tambah','App\Http\Controllers\InventarisController@new_inventaris_masuk')->name('inventarisTambah');
    Route::post('/inventaris/masuk/tambah','App\Http\Controllers\InventarisController@inventarisMasukPost')->name('inventarisMasukPost');
    Route::get('/inventaris/masuk/update/{id}','App\Http\Controllers\InventarisController@update_inventaris_masuk')->name('inventarisUpdate');
    Route::post('/inventaris/masuk/update/','App\Http\Controllers\InventarisController@inventarisMasukUpdatePost')->name('inventarisMasukUpdatePost');
    Route::get('/inventaris/masuk/delete/{id}',[\App\Http\Controllers\InventarisController::class, 'inventarisMasukDelete'])->name('inventarisMasukDelete');

    Route::get('/inventaris/keluar','App\Http\Controllers\InventarisController@table_datatable_basic_2')->name('inventarisKeluar');
    Route::get('/inventaris/keluar/tambah','App\Http\Controllers\InventarisController@new_inventaris_keluar')->name('inventarisTambahKeluar');
    Route::post('/inventaris/keluar/tambah','App\Http\Controllers\InventarisController@inventarisKeluarPost')->name('inventarisKeluarPost');
    Route::get('/inventaris/keluar/update/{id}','App\Http\Controllers\InventarisController@update_inventaris_keluar')->name('inventarisUpdateKeluar');
    Route::post('/inventaris/keluar/update','App\Http\Controllers\InventarisController@inventarisKeluarUpdatePost')->name('inventarisKeluarUpdatePost');
    Route::get('/inventaris/keluar/delete/{id}',[\App\Http\Controllers\InventarisController::class, 'inventarisKeluarDelete'])->name('inventarisKeluarDelete');

    Route::get('/inventaris/stok','App\Http\Controllers\InventarisController@stok')->name('inventarisStok');

    Route::get('/inventaris/operasional','App\Http\Controllers\InventarisController@inventarisOperasional')->name('inventarisOperasional');
    Route::get('/inventaris/operasional/tambah','App\Http\Controllers\InventarisController@inventarisOperasionalNew')->name('inventarisOperasionalNew');
    Route::post('/inventaris/operasional/tambah','App\Http\Controllers\InventarisController@inventarisOperasionalNewPost')->name('inventarisOperasionalNewPost');
    Route::get('/inventaris/operasional/update/{id}','App\Http\Controllers\InventarisController@inventarisOperasionalUpdate')->name('inventarisOperasionalUpdate');
    Route::post('/inventaris/operasional/update','App\Http\Controllers\InventarisController@inventarisOperasionalUpdatePost')->name('inventarisOperasionalUpdatePost');
    Route::get('/inventaris/operasional/delete/{id}',[\App\Http\Controllers\InventarisController::class, 'inventarisOperasionalDelete'])->name('inventarisOperasionalDelete');


    Route::get('/pembeli/pembelian','App\Http\Controllers\PembeliController@daftarPembeli')->name('daftarPembeli');
    Route::get('/pembeli/pembelian/tambahkavling','App\Http\Controllers\PembeliController@tambahPembeli')->name('tambahPembeliKavling');
    Route::get('/pembeli/pembelian/tambahbangunancash','App\Http\Controllers\PembeliController@tambahPembeliBangunanCash')->name('tambahPembeliBangunanCash');
    Route::get('/pembeli/pembelian/tambahbangunankredit','App\Http\Controllers\PembeliController@tambahPembeliBangunanKredit')->name('tambahPembeliBangunanKredit');
    Route::post('/pembeli/pembelian/tambah','App\Http\Controllers\PembeliController@pembeliNewPost')->name('pembeliNewPost');
    Route::post('/pembeli/pembelian/tambahbangunancash','App\Http\Controllers\PembeliController@pembeliBangunanCashNewPost')->name('pembeliBangunanCashNewPost');
    Route::post('/pembeli/pembelian/tambahbangunankredit','App\Http\Controllers\PembeliController@pembeliBangunanKreditNewPost')->name('pembeliBangunanKreditNewPost');
    Route::get('/pembeli/pembelian/view/{id}','App\Http\Controllers\PembeliController@viewPembeli')->name('viewPembeli');
    Route::get('/pembeli/pembelian/update/{id}','App\Http\Controllers\PembeliController@updatePembeli')->name('updatePembeli');
    Route::post('/pembeli/pembelian/update','App\Http\Controllers\PembeliController@pembeliUpdatePost')->name('pembeliUpdatePost');
    Route::post('/pembeli/pembelian/updatebangunancash','App\Http\Controllers\PembeliController@pembeliUpdateBangunanCashPost')->name('pembeliUpdateBangunanCashPost');
    Route::post('/pembeli/pembelian/updatebangunankredit','App\Http\Controllers\PembeliController@pembeliUpdateBangunanKreditPost')->name('pembeliUpdateBangunanKreditPost');
    Route::get('/pembeli/pembelian/delete/{id}',[\App\Http\Controllers\PembeliController::class, 'pembelianDelete'])->name('pembelianDelete');

    Route::get('/pembeli/pengembalian',[\App\Http\Controllers\PembeliController::class,'pengembalianList'])->name('pengembalianList');
    Route::get('/pembeli/pengembalian/tambah',[\App\Http\Controllers\PembeliController::class,'pengembalianNew'])->name('pengembalianNew');
    Route::post('/pembeli/pengembalian/tambah',[\App\Http\Controllers\PembeliController::class,'pengembalianNewPost'])->name('pengembalianNewPost');
    Route::get('/pembeli/pengembalian/view/{id}',[\App\Http\Controllers\PembeliController::class, 'pengembalianView'])->name('pengembalianView');
    Route::get('/pembeli/pengembalian/update/{id}',[\App\Http\Controllers\PembeliController::class, 'pengembalianUpdate'])->name('pengembalianUpdate');
    Route::post('/pembeli/pengembalian/update',[\App\Http\Controllers\PembeliController::class, 'pengembalianUpdatePost'])->name('pengembalianUpdatePost');
    Route::get('/pembeli/pengembalian/{id}',[\App\Http\Controllers\PembeliController::class, 'pengembalianDelete'])->name('pengembalianDelete');

    Route::get('/pembayaran','App\Http\Controllers\PembayaranController@listPembayaran')->name('pembayaranList');
    Route::get('/pembayaran/tambah','App\Http\Controllers\PembayaranController@pembayaranCicilan')->name('pembayaranCicilan');
    Route::post('/pembayaran/tambah','App\Http\Controllers\PembayaranController@pembayaranNewPost')->name('pembayaranNewPost');
    Route::get('/pembayaran/update/{id}','App\Http\Controllers\PembayaranController@pembayaranUpdate')->name('pembayaranUpdate');
    Route::post('/pembayaran/update','App\Http\Controllers\PembayaranController@pembayaranUpdatePost')->name('pembayaranUpdatePost');
    Route::get('/pembayaran/delete/{id}',[\App\Http\Controllers\PembayaranController::class, 'pembayaranDelete'])->name('pembayaranDelete');

    Route::get('/penggajian',[\App\Http\Controllers\Controller::class, 'redirect503']);

    Route::get('/keluhan/','App\Http\Controllers\KeluhanController@keluhanList')->name('keluhanList');
    Route::get('/keluhan/tambah','App\Http\Controllers\KeluhanController@keluhanTambah')->name('keluhanTambah');
    Route::post('/keluhan/tambah','App\Http\Controllers\KeluhanController@keluhanPostNew')->name('keluhanPostNew');
    Route::get('/keluhan/view/{id}','App\Http\Controllers\KeluhanController@keluhanView')->name('keluhanView');
    Route::get('/keluhan/update/{id}','App\Http\Controllers\KeluhanController@keluhanUpdate')->name('keluhanUpdate');
    Route::post('/keluhan/update/','App\Http\Controllers\KeluhanController@keluhanPostUpdate')->name('keluhanPostUpdate');
    Route::get('/keluhan/delete/{id}',[\App\Http\Controllers\KeluhanController::class, 'keluhanDelete'])->name('keluhanDelete');

    Route::get('/user/','App\Http\Controllers\UserController@userList')->name('userList');
    Route::get('/user/tambah','App\Http\Controllers\UserController@userTambah')->name('userTambah');
    Route::post('/user/tambah','App\Http\Controllers\UserController@userPostNew')->name('userPostNew');
    Route::get('/user/view/{id}','App\Http\Controllers\UserController@userView')->name('userView');
    Route::get('/user/update/{id}','App\Http\Controllers\UserController@userUpdate')->name('userUpdate');
    Route::post('/user/update/','App\Http\Controllers\UserController@userPostUpdate')->name('userPostUpdate');
    Route::get('/user/delete/{id}',[\App\Http\Controllers\UserController::class, 'userDelete'])->name('userDelete');

    Route::get('/logout',function (){
        session()->flush();
       \Illuminate\Support\Facades\Auth::logout();
       return redirect()->route('login');
    });
});

Route::get('/index', 'App\Http\Controllers\OmahadminController@dashboard_1');
Route::get('/analytics', 'App\Http\Controllers\OmahadminController@analytics');
Route::get('/customer-list', 'App\Http\Controllers\OmahadminController@customer_list');
Route::get('/property-details', 'App\Http\Controllers\OmahadminController@property_details');
Route::get('/order-list', 'App\Http\Controllers\OmahadminController@order_list');
Route::get('/review', 'App\Http\Controllers\OmahadminController@review');
Route::get('/app-calender', 'App\Http\Controllers\OmahadminController@app_calender');
Route::get('/app-profile', 'App\Http\Controllers\OmahadminController@app_profile');
Route::get('/post-details', 'App\Http\Controllers\OmahadminController@post_details');
Route::get('/chart-chartist', 'App\Http\Controllers\OmahadminController@chart_chartist');
Route::get('/chart-chartjs', 'App\Http\Controllers\OmahadminController@chart_chartjs');
Route::get('/chart-flot', 'App\Http\Controllers\OmahadminController@chart_flot');
Route::get('/chart-morris', 'App\Http\Controllers\OmahadminController@chart_morris');
Route::get('/chart-peity', 'App\Http\Controllers\OmahadminController@chart_peity');
Route::get('/chart-sparkline', 'App\Http\Controllers\OmahadminController@chart_sparkline');
Route::get('/ecom-checkout', 'App\Http\Controllers\OmahadminController@ecom_checkout');
Route::get('/ecom-customers', 'App\Http\Controllers\OmahadminController@ecom_customers');
Route::get('/ecom-invoice', 'App\Http\Controllers\OmahadminController@ecom_invoice');
Route::get('/ecom-product-detail', 'App\Http\Controllers\OmahadminController@ecom_product_detail');
Route::get('/ecom-product-grid', 'App\Http\Controllers\OmahadminController@ecom_product_grid');
Route::get('/ecom-product-list', 'App\Http\Controllers\OmahadminController@ecom_product_list');
Route::get('/ecom-product-order', 'App\Http\Controllers\OmahadminController@ecom_product_order');
Route::get('/email-compose', 'App\Http\Controllers\OmahadminController@email_compose');
Route::get('/email-inbox', 'App\Http\Controllers\OmahadminController@email_inbox');
Route::get('/email-read', 'App\Http\Controllers\OmahadminController@email_read');
Route::get('/form-editor-summernote', 'App\Http\Controllers\OmahadminController@form_editor_summernote');
Route::get('/form-element', 'App\Http\Controllers\OmahadminController@form_element');
Route::get('/form-pickers', 'App\Http\Controllers\OmahadminController@form_pickers');
Route::get('/form-validation-jquery', 'App\Http\Controllers\OmahadminController@form_validation_jquery');
Route::get('/form-wizard', 'App\Http\Controllers\OmahadminController@form_wizard');
Route::get('/map-jqvmap', 'App\Http\Controllers\OmahadminController@map_jqvmap');
Route::get('/page-error-400', 'App\Http\Controllers\OmahadminController@page_error_400');
Route::get('/page-error-403', 'App\Http\Controllers\OmahadminController@page_error_403');
Route::get('/page-error-404', 'App\Http\Controllers\OmahadminController@page_error_404');
Route::get('/page-error-500', 'App\Http\Controllers\OmahadminController@page_error_500');
Route::get('/page-error-503', 'App\Http\Controllers\OmahadminController@page_error_503');
Route::get('/page-forgot-password', 'App\Http\Controllers\OmahadminController@page_forgot_password');
Route::get('/page-lock-screen', 'App\Http\Controllers\OmahadminController@page_lock_screen');
Route::get('/page-login', 'App\Http\Controllers\OmahadminController@page_login');
Route::get('/page-register', 'App\Http\Controllers\OmahadminController@page_register');
Route::get('/table-bootstrap-basic', 'App\Http\Controllers\OmahadminController@table_bootstrap_basic');
Route::get('/table-datatable-basic', 'App\Http\Controllers\OmahadminController@table_datatable_basic');
Route::get('/uc-lightgallery', 'App\Http\Controllers\OmahadminController@uc_lightgallery');
Route::get('/uc-nestable', 'App\Http\Controllers\OmahadminController@uc_nestable');
Route::get('/uc-noui-slider', 'App\Http\Controllers\OmahadminController@uc_noui_slider');
Route::get('/uc-select2', 'App\Http\Controllers\OmahadminController@uc_select2');
Route::get('/uc-sweetalert', 'App\Http\Controllers\OmahadminController@uc_sweetalert');
Route::get('/uc-toastr', 'App\Http\Controllers\OmahadminController@uc_toastr');
Route::get('/ui-accordion', 'App\Http\Controllers\OmahadminController@ui_accordion');
Route::get('/ui-alert', 'App\Http\Controllers\OmahadminController@ui_alert');
Route::get('/ui-badge', 'App\Http\Controllers\OmahadminController@ui_badge');
Route::get('/ui-button', 'App\Http\Controllers\OmahadminController@ui_button');
Route::get('/ui-button-group', 'App\Http\Controllers\OmahadminController@ui_button_group');
Route::get('/ui-card', 'App\Http\Controllers\OmahadminController@ui_card');
Route::get('/ui-carousel', 'App\Http\Controllers\OmahadminController@ui_carousel');
Route::get('/ui-dropdown', 'App\Http\Controllers\OmahadminController@ui_dropdown');
Route::get('/ui-grid', 'App\Http\Controllers\OmahadminController@ui_grid');
Route::get('/ui-list-group', 'App\Http\Controllers\OmahadminController@ui_list_group');
Route::get('/ui-media-object', 'App\Http\Controllers\OmahadminController@ui_media_object');
Route::get('/ui-modal', 'App\Http\Controllers\OmahadminController@ui_modal');
Route::get('/ui-pagination', 'App\Http\Controllers\OmahadminController@ui_pagination');
Route::get('/ui-popover', 'App\Http\Controllers\OmahadminController@ui_popover');
Route::get('/ui-progressbar', 'App\Http\Controllers\OmahadminController@ui_progressbar');
Route::get('/ui-tab', 'App\Http\Controllers\OmahadminController@ui_tab');
Route::get('/ui-typography', 'App\Http\Controllers\OmahadminController@ui_typography');
Route::get('/widget-basic', 'App\Http\Controllers\OmahadminController@widget_basic');
