<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelians', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('alamat_id');
            $table->foreign('alamat_id')->references('id')->on('alamats');
            $table->unsignedBigInteger('tiperumah_id');
            $table->foreign('tiperumah_id')->references('id')->on('tiperumahs');
            $table->unsignedBigInteger('pilihanpembayaran_id');
            $table->foreign('pilihanpembayaran_id')->references('id')->on('pilihanpembayarans');
            $table->string('nama_pembeli')->nullable();
            $table->string('alamat')->nullable();
            $table->string('nomor_handphone')->nullable();
            $table->string('harga')->nullable();
            $table->string('dp')->nullable();
            $table->string('tanggal_pembayaran')->nullable();
            $table->string('tanggal_akad')->nullable();
            $table->string('jenis_pembayaran')->nullable();
            $table->string('no_kwitansi')->nullable();
            $table->string('ktp')->nullable();
            $table->string('npwp')->nullable();
            $table->string('surat_nikah')->nullable();
            $table->string('surat_akta')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at',0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelians');
    }
}
