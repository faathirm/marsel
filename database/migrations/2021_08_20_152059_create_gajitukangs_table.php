<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGajitukangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gajitukangs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tukang_id');
            $table->foreign('tukang_id')->references('id')->on('tukangs');
            $table->unsignedBigInteger('alamat_id');
            $table->foreign('alamat_id')->references('id')->on('alamats');
            $table->string('jasa')->nullable();
            $table->string('total_pengerjaan')->nullable();
            $table->string('kasbon')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('total')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at',0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gajitukangs');
    }
}
