<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Daftarpengembalian;
use App\Models\Pembayaran;
use App\Models\Pembelian;
use App\Models\Pengembalian;
use App\Models\Pilihanpembayaran;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PembeliController extends Controller
{
    function daftarPembeli()
    {
        $page_title = 'Data Pembeli';
        $page_description = '';

        $action = __FUNCTION__;

        $pembelian = Pembelian::where('deleted_at', null)->get();

        return view('pembeli.list', compact('page_title', 'page_description', 'action', 'pembelian'));
    }

    function tambahPembeli()
    {
        $page_title = 'Tambah Pembeli';
        $page_description = '';
        $isView = false;
        $isUpdate = false;

        $action = __FUNCTION__;

        $alamats = Alamat::where('status', 'tersedia')->where('deleted_at', null)->get();
        $pilihanpembayarans = Pilihanpembayaran::where('deleted_at', null)->get();

        return view('pembeli.form', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'alamats', 'pilihanpembayarans'));
    }

    function tambahPembeliBangunanCash()
    {
        $page_title = 'Tambah Pembeli';
        $page_description = '';
        $isView = false;
        $isUpdate = false;

        $action = __FUNCTION__;

        $alamats = Alamat::where('status', 'tersedia')->where('deleted_at', null)->get();
        $pilihanpembayarans = Pilihanpembayaran::where('deleted_at', null)->get();

        return view('pembeli.bangunancash', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'alamats', 'pilihanpembayarans'));
    }

    function tambahPembeliBangunanKredit()
    {
        $page_title = 'Tambah Pembeli';
        $page_description = '';
        $isView = false;
        $isUpdate = false;

        $action = __FUNCTION__;

        $alamats = Alamat::where('status', 'tersedia')->where('deleted_at', null)->get();
        $pilihanpembayarans = Pilihanpembayaran::where('deleted_at', null)->where('id', '!=', '1')->get();

        return view('pembeli.bangunankredit', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'alamats', 'pilihanpembayarans'));
    }

    function viewPembeli($id)
    {
        $page_title = 'View Pembeli';
        $page_description = '';
        $isView = true;
        $isUpdate = false;

        $action = __FUNCTION__;

        $pembeli = Pembelian::find($id);
        $alamats = Alamat::where('status', 'tersedia')->where('deleted_at', null)->get();
        if ($pembeli->jenis == "kavling") {
            return view('pembeli.form', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'pembeli', 'alamats'));
        } else if ($pembeli->jenis == "bangunan" && $pembeli->dp == 0) {
            return view('pembeli.bangunancash', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'pembeli', 'alamats'));
        } else {
            return view('pembeli.bangunankredit', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'pembeli', 'alamats'));
        }
    }

    function updatePembeli($id)
    {
        $page_title = 'View Pembeli';
        $page_description = '';
        $isView = false;
        $isUpdate = true;

        $action = __FUNCTION__;
        $pembeli = Pembelian::find($id);
        $alamats = Alamat::where('status', 'tersedia')->where('deleted_at', null)->get();
        if ($pembeli->jenis == "kavling") {
            return view('pembeli.form', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'pembeli', 'alamats'));
        } else if ($pembeli->jenis == "bangunan" && $pembeli->dp == 0) {
            return view('pembeli.bangunancash', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'pembeli', 'alamats'));
        } else {
            return view('pembeli.bangunankredit', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate', 'pembeli', 'alamats'));
        }

        return view('pembeli.form', compact('page_title', 'page_description', 'action', 'isView', 'isUpdate'));
    }

    function pembeliNewPost(Request $request)
    {
        //dd($request);
        $pembeli = new Pembelian();
        $pembeli->alamat_id = $request->alamat_id;
        $pembeli->pilihanpembayaran_id = 0;
        $pembeli->jenis = "kavling";
        $pembeli->luas_tanah = $request->luas_tanah_kavling;
        $pembeli->luas_bangunan = 0;
        $pembeli->harga_meter = 0;
        $pembeli->nama_pembeli = $request->nama_pembeli;
        $pembeli->nik = $request->nik;
        $pembeli->alamat = $request->alamat;
        $pembeli->nomor_handphone = $request->no_handphone;
        $pembeli->harga = numberOnly($request->harga_kavling);
        $pembeli->dp = 0;
        $pembeli->tanggal_pembayaran = $request->tanggal_pembayaran_kavling;
        $pembeli->tanggal_akad = $request->tanggal_akad_kavling;
        $pembeli->jenis_pembayaran = $request->jenis_pembayaran_kavling;
        $pembeli->no_kwitansi = $request->no_kwitansi_kavling;

        $ktp = $request->file('ktp');
        $ktp_destination = 'doc/ktp/';
        $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
        $ktp->move($ktp_destination, $ktp_name);

        $npwp = $request->file('npwp');
        $npwp_destination = 'doc/npwp/';
        $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
        $npwp->move($npwp_destination, $npwp_name);

        $sn = $request->file('surat_nikah');
        $sn_destination = 'doc/suratnikah/';
        $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
        $sn->move($sn_destination, $sn_name);

        $sa = $request->file('surat_akad');
        $sa_destination = 'doc/suratakad/';
        $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
        $sa->move($sa_destination, $sa_name);

        $pembeli->ktp = $ktp_name;
        $pembeli->npwp = $npwp_name;
        $pembeli->surat_nikah = $sn_name;
        $pembeli->surat_akta = $sa_name;

        $pembeli->save();

        $alamat = Alamat::find($request->alamat_id);
        $alamat->status = "Tidak Tersedia";
        $alamat->save();

        return redirect()->route('daftarPembeli');
    }

    function pembeliBangunanCashNewPost(Request $request)
    {
        //dd($request);
        $pembeli = new Pembelian();
        $pembeli->alamat_id = $request->alamat_id;
        $pembeli->pilihanpembayaran_id = 0;
        $pembeli->jenis = "bangunan";
        $pembeli->luas_tanah = $request->luas_tanah_rumah;
        $pembeli->luas_bangunan = $request->luas_bangunan_rumah;
        $pembeli->harga_meter = numberOnly($request->harga_meter_rumah);
        $pembeli->nama_pembeli = $request->nama_pembeli;
        $pembeli->nik = $request->nik;
        $pembeli->alamat = $request->alamat;
        $pembeli->nomor_handphone = $request->no_handphone;
        $pembeli->harga = numberOnly($request->harga_rumah);
        $pembeli->dp = 0;
        $pembeli->tanggal_pembayaran = $request->tanggal_pembayaran_rumah;
        $pembeli->tanggal_akad = $request->tanggal_akad_rumah;
        $pembeli->jenis_pembayaran = $request->jenis_pembayaran_rumah;
        $pembeli->no_kwitansi = $request->no_kwitansi_rumah;

        $ktp = $request->file('ktp');
        $ktp_destination = 'doc/ktp/';
        $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
        $ktp->move($ktp_destination, $ktp_name);

        $npwp = $request->file('npwp');
        $npwp_destination = 'doc/npwp/';
        $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
        $npwp->move($npwp_destination, $npwp_name);

        $sn = $request->file('surat_nikah');
        $sn_destination = 'doc/suratnikah/';
        $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
        $sn->move($sn_destination, $sn_name);

        $sa = $request->file('surat_akad');
        $sa_destination = 'doc/suratakad/';
        $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
        $sa->move($sa_destination, $sa_name);

        $pembeli->ktp = $ktp_name;
        $pembeli->npwp = $npwp_name;
        $pembeli->surat_nikah = $sn_name;
        $pembeli->surat_akta = $sa_name;

        $pembeli->save();

        $alamat = Alamat::find($request->alamat_id);
        $alamat->status = "Tidak Tersedia";
        $alamat->save();

        return redirect()->route('daftarPembeli');
    }

    public function pembeliBangunanKreditNewPost(Request $request)
    {
        //dd($request);
        $pembeli = new Pembelian();
        $pembeli->alamat_id = $request->alamat_id;
        $pembeli->pilihanpembayaran_id = $request->pilihanpembayaran_id_rumah;
        $pembeli->jenis = "bangunan";
        $pembeli->luas_tanah = $request->luas_tanah_rumah;
        $pembeli->luas_bangunan = $request->luas_bangunan_rumah;
        $pembeli->harga_meter = numberOnly($request->harga_meter_rumah);
        $pembeli->nama_pembeli = $request->nama_pembeli;
        $pembeli->nik = $request->nik;
        $pembeli->alamat = $request->alamat;
        $pembeli->nomor_handphone = $request->no_handphone;
        $pembeli->harga = numberOnly($request->total_kredit_rumah);
        $pembeli->dp = numberOnly($request->dp_rumah);
        $pembeli->tanggal_pembayaran = $request->tanggal_pembayaran_rumah;
        $pembeli->tanggal_akad = $request->tanggal_akad_rumah;
        $pembeli->jenis_pembayaran = $request->jenis_pembayaran_rumah;
        $pembeli->no_kwitansi = $request->no_kwitansi_rumah;

        $ktp = $request->file('ktp');
        $ktp_destination = 'doc/ktp/';
        $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
        $ktp->move($ktp_destination, $ktp_name);

        $npwp = $request->file('npwp');
        $npwp_destination = 'doc/npwp/';
        $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
        $npwp->move($npwp_destination, $npwp_name);

        $sn = $request->file('surat_nikah');
        $sn_destination = 'doc/suratnikah/';
        $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
        $sn->move($sn_destination, $sn_name);

        $sa = $request->file('surat_akad');
        $sa_destination = 'doc/suratakad/';
        $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
        $sa->move($sa_destination, $sa_name);

        $pembeli->ktp = $ktp_name;
        $pembeli->npwp = $npwp_name;
        $pembeli->surat_nikah = $sn_name;
        $pembeli->surat_akta = $sa_name;

        $pembeli->save();

        $alamat = Alamat::find($request->alamat_id);
        $alamat->status = "Tidak Tersedia";
        $alamat->save();

        return redirect()->route('daftarPembeli');
    }

    public function pembeliUpdatePost(Request $request)
    {
        $pembeli = Pembelian::find($request->id);
        $pembeli->alamat_id = $request->alamat_id;
        $pembeli->pilihanpembayaran_id = 0;
        $pembeli->jenis = "kavling";
        $pembeli->luas_tanah = $request->luas_tanah_kavling;
        $pembeli->luas_bangunan = 0;
        $pembeli->harga_meter = 0;
        $pembeli->nama_pembeli = $request->nama_pembeli;
        $pembeli->nik = $request->nik;
        $pembeli->alamat = $request->alamat;
        $pembeli->nomor_handphone = $request->no_handphone;
        $pembeli->harga = numberOnly($request->harga_kavling);
        $pembeli->dp = 0;
        $pembeli->tanggal_pembayaran = $request->tanggal_pembayaran_kavling;
        $pembeli->tanggal_akad = $request->tanggal_akad_kavling;
        $pembeli->jenis_pembayaran = $request->jenis_pembayaran_kavling;
        $pembeli->no_kwitansi = $request->no_kwitansi_kavling;

        if ($request->file('ktp')) {
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pembeli->ktp = $ktp_name;
        }

        if ($request->file('npwp')) {
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pembeli->npwp = $npwp_name;
        }

        if ($request->file('surat_nikah')) {
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/suratnikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pembeli->surat_nikah = $sn_name;
        }

        if ($request->file('surat_akad')) {
            $sa = $request->file('surat_akad');
            $sa_destination = 'doc/suratakad/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pembeli->surat_akta = $sa_name;
        }

        if ($pembeli->alamat_id != $request->alamat_id) {
            $alamat = Alamat::find($pembeli->alamat_id);
            $alamat->status = "Tersedia";
            $alamat->save();
        }

        $alamat = Alamat::find($request->alamat_id);
        $alamat->status = "Tidak Tersedia";
        $alamat->save();

        $pembeli->save();

        return redirect()->route('daftarPembeli');
    }

    public function pembeliUpdateBangunanCashPost(Request $request)
    {
        //dd($request);
        $pembeli = Pembelian::find($request->id);
        $pembeli->alamat_id = $request->alamat_id;
        $pembeli->pilihanpembayaran_id = 0;
        $pembeli->jenis = "bangunan";
        $pembeli->luas_tanah = $request->luas_tanah_rumah;
        $pembeli->luas_bangunan = $request->luas_bangunan_rumah;
        $pembeli->harga_meter = numberOnly($request->harga_meter_rumah);
        $pembeli->nama_pembeli = $request->nama_pembeli;
        $pembeli->nik = $request->nik;
        $pembeli->alamat = $request->alamat;
        $pembeli->nomor_handphone = $request->no_handphone;
        $pembeli->harga = numberOnly($request->harga_rumah);
        $pembeli->dp = 0;
        $pembeli->tanggal_pembayaran = $request->tanggal_pembayaran_rumah;
        $pembeli->tanggal_akad = $request->tanggal_akad_rumah;
        $pembeli->jenis_pembayaran = $request->jenis_pembayaran_rumah;
        $pembeli->no_kwitansi = $request->no_kwitansi_rumah;

        if ($request->file('ktp')) {
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pembeli->ktp = $ktp_name;
        }

        if ($request->file('npwp')) {
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pembeli->npwp = $npwp_name;
        }

        if ($request->file('surat_nikah')) {
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/suratnikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pembeli->surat_nikah = $sn_name;
        }

        if ($request->file('surat_akad')) {
            $sa = $request->file('surat_akad');
            $sa_destination = 'doc/suratakad/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pembeli->surat_akta = $sa_name;
        }

        if ($pembeli->alamat_id != $request->alamat_id) {
            $alamat = Alamat::find($pembeli->alamat_id);
            $alamat->status = "Tersedia";
            $alamat->save();
        }

        $alamat = Alamat::find($request->alamat_id);
        $alamat->status = "Tidak Tersedia";
        $alamat->save();

        $pembeli->save();

        return redirect()->route('daftarPembeli');
    }

    public function pembeliUpdateBangunanKreditPost(Request $request)
    {
        $pembeli = Pembelian::find($request->id);
        $pembeli->alamat_id = $request->alamat_id;
        $pembeli->pilihanpembayaran_id = $request->pilihanpembayaran_id_rumah;
        $pembeli->jenis = "bangunan";
        $pembeli->luas_tanah = $request->luas_tanah_rumah;
        $pembeli->luas_bangunan = $request->luas_bangunan_rumah;
        $pembeli->harga_meter = numberOnly($request->harga_meter_rumah);
        $pembeli->nama_pembeli = $request->nama_pembeli;
        $pembeli->nik = $request->nik;
        $pembeli->alamat = $request->alamat;
        $pembeli->nomor_handphone = $request->no_handphone;
        $pembeli->harga = numberOnly($request->total_kredit_rumah);
        $pembeli->dp = numberOnly($request->dp_rumah);
        $pembeli->tanggal_pembayaran = $request->tanggal_pembayaran_rumah;
        $pembeli->tanggal_akad = $request->tanggal_akad_rumah;
        $pembeli->jenis_pembayaran = $request->jenis_pembayaran_rumah;
        $pembeli->no_kwitansi = $request->no_kwitansi_rumah;

        if ($request->file('ktp')) {
            $ktp = $request->file('ktp');
            $ktp_destination = 'doc/ktp/';
            $ktp_name = date('YmdHis') . "." . $ktp->getClientOriginalExtension();
            $ktp->move($ktp_destination, $ktp_name);
            $pembeli->ktp = $ktp_name;
        }

        if ($request->file('npwp')) {
            $npwp = $request->file('npwp');
            $npwp_destination = 'doc/npwp/';
            $npwp_name = date('YmdHis') . "." . $npwp->getClientOriginalExtension();
            $npwp->move($npwp_destination, $npwp_name);
            $pembeli->npwp = $npwp_name;
        }

        if ($request->file('surat_nikah')) {
            $sn = $request->file('surat_nikah');
            $sn_destination = 'doc/suratnikah/';
            $sn_name = date('YmdHis') . "." . $sn->getClientOriginalExtension();
            $sn->move($sn_destination, $sn_name);
            $pembeli->surat_nikah = $sn_name;
        }

        if ($request->file('surat_akad')) {
            $sa = $request->file('surat_akad');
            $sa_destination = 'doc/suratakad/';
            $sa_name = date('YmdHis') . "." . $sa->getClientOriginalExtension();
            $sa->move($sa_destination, $sa_name);
            $pembeli->surat_akta = $sa_name;
        }

        if ($pembeli->alamat_id != $request->alamat_id) {
            $alamat = Alamat::find($pembeli->alamat_id);
            $alamat->status = "Tersedia";
            $alamat->save();
        }

        $alamat = Alamat::find($request->alamat_id);
        $alamat->status = "Tidak Tersedia";
        $alamat->save();

        $pembeli->save();

        return redirect()->route('daftarPembeli');
    }

    public function pengembalianList()
    {
        $page_title = 'Data Pengembalian';
        $page_description = '';

        $action = __FUNCTION__;

        $pengembalians = Pengembalian::where('deleted_at', null)->get();

        return view('pembeli.pengembalian.list', compact('page_title', 'page_description', 'action', 'pengembalians'));
    }

    public function pengembalianNew()
    {
        $page_title = 'Tambah Data Pengembalian';
        $page_description = '';
        $isUpdate = false;
        $isView = false;
        $action = __FUNCTION__;
        $pembelians = Pembelian::where('deleted_at', null)->where('pilihanpembayaran_id','!=','0')->get();
        return view('pembeli.pengembalian.form', compact('page_title', 'page_description','action','isUpdate','isView','pembelians'));
    }

    public function pengembalianNewPost(Request $request)
    {
        $pengembalians = new Pengembalian();
        $pengembalians->pembelian_id = $request->pembelian_id;
        $pengembalians->keterangan = $request->keterangan;
        $pengembalians->save();
        for($x=0;$x<count($request->tanggalPengembalian);$x++){
            $dp = new Daftarpengembalian();
            $dp->pengembalian_id = $pengembalians->id;
            $dp->tanggal = $request->tanggalPengembalian[$x];
            $dp->jumlah = numberOnly($request->jumlahPengembalian[$x]);
            $dp->save();
        }
        return redirect()->route('pengembalianList');
    }

    public function pengembalianView($id)
    {
        $page_title = 'View Data Pengembalian';
        $page_description = '';
        $isUpdate = false;
        $isView = true;
        $action = __FUNCTION__;
        $pengembalians = Pengembalian::find($id);
        $pembelians = Pembelian::where('deleted_at', null)->where('pilihanpembayaran_id','!=','0')->get();
        $daftarpengembalians = Daftarpengembalian::where('pengembalian_id',$pengembalians->id)->get();

        //hitung
        $pembelian = Pembelian::find($pengembalians->pembelian_id);
        $total = Pembayaran::where('pembelian_id',$pengembalians->pembelian_id)->where('jenis','cicilan')->sum('total_pembayaran');
        $totalyangsudahdibayarkan = $pembelian->dp + $total;
        $totalpengembalian = Daftarpengembalian::where('pengembalian_id',$pengembalians->id)->sum('jumlah');
        $sisa = $totalyangsudahdibayarkan-$totalpengembalian;

        return view('pembeli.pengembalian.form', compact('page_title', 'page_description','action','isUpdate','isView','pembelians','pengembalians','totalyangsudahdibayarkan','daftarpengembalians','totalpengembalian','sisa'));
    }

    public function pengembalianUpdate($id)
    {
        $page_title = 'Update Data Pengembalian';
        $page_description = '';
        $isUpdate = true;
        $isView = false;
        $action = __FUNCTION__;
        $pengembalians = Pengembalian::find($id);
        $pembelians = Pembelian::where('deleted_at', null)->where('pilihanpembayaran_id','!=','0')->get();
        $daftarpengembalians = Daftarpengembalian::where('pengembalian_id',$pengembalians->id)->get();

        //hitung
        $pembelian = Pembelian::find($pengembalians->pembelian_id);
        $total = Pembayaran::where('pembelian_id',$pengembalians->pembelian_id)->where('jenis','cicilan')->sum('total_pembayaran');
        $totalyangsudahdibayarkan = $pembelian->dp + $total;
        $totalpengembalian = Daftarpengembalian::where('pengembalian_id',$pengembalians->id)->sum('jumlah');
        $sisa = $totalyangsudahdibayarkan-$totalpengembalian;

        return view('pembeli.pengembalian.form', compact('page_title', 'page_description','action','isUpdate','isView','pembelians','pengembalians','totalyangsudahdibayarkan','daftarpengembalians','totalpengembalian','sisa'));
    }

    public function pengembalianUpdatePost(Request $request)
    {
        $pengembalians = Pengembalian::find($request->id);
        $pengembalians->pembelian_id = $request->pembelian_id;
        $pengembalians->keterangan = $request->keterangan;
        $pengembalians->save();
        $delete = Daftarpengembalian::where('pengembalian_id',$request->id)->delete();
        for($x=0;$x<count($request->tanggalPengembalian);$x++){
            $dp = new Daftarpengembalian();
            $dp->pengembalian_id = $pengembalians->id;
            $dp->tanggal = $request->tanggalPengembalian[$x];
            $dp->jumlah = numberOnly($request->jumlahPengembalian[$x]);
            $dp->save();
        }
        return redirect()->route('pengembalianList');
    }

    public function pembelianDelete($id){
        $delete = Pembelian::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->daftarPembeli();
    }

    public function pengembalianDelete($id){
        $delete = Pengembalian::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->pengembalianList();
    }

}
