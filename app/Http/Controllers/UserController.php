<?php

namespace App\Http\Controllers;

use App\Models\Barokahuser;
use App\Models\Barokahuseracl;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function userList()
    {
        $page_title = 'Daftar Pengguna';
        $page_description = '';

        $action = __FUNCTION__;

        $users = Barokahuser::where('deleted_at',null)->get();

        return view('user.list', compact('page_title', 'page_description','action','users'));
    }

    function userTambah()
    {
        $page_title = 'Tambah Pengguna';
        $page_description = '';
        $isUpdate = false;
        $isView = false;
        $action = __FUNCTION__;
        return view('user.form', compact('page_title', 'page_description','action','isUpdate','isView'));
    }

    function userView($id)
    {
        $page_title = 'Lihat Pengguna';
        $page_description = '';
        $isUpdate = false;
        $isView = true;
        $action = __FUNCTION__;
        $user = Barokahuser::find($id);
        $acl = Barokahuseracl::where('user_id',$id)->get();
        return view('user.form', compact('page_title', 'page_description','action','isUpdate','isView','user','acl'));
    }

    function userUpdate($id)
    {
        $page_title = 'Update Pengguna';
        $page_description = '';
        $isUpdate = true;
        $isView = false;
        $action = __FUNCTION__;
        $user = Barokahuser::find($id);
        $acl = Barokahuseracl::where('user_id',$id)->get();
        return view('user.form', compact('page_title', 'page_description','action','isUpdate','isView','user','acl'));
    }

    function userPostNew(Request $request)
    {
        //dd($request);
        $user = new Barokahuser();
        $user->username = $request->username;
        $user->nama = $request->nama;
        $user->password = Hash::make($request->password);
        $user->save();
        foreach($request->acl as $acls){
            $acl = new Barokahuseracl();
            $acl->user_id = $user->id;
            $acl->access = $acls;
            $acl->save();
        }
        session()->flash('submission','success');

        return $this->userList();
    }

    function userPostUpdate(Request $request)
    {
        $user = Barokahuser::find($request->id);
        $user->username = $request->username;
        $user->nama = $request->nama;
        if($request->password != null){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        $delete = Barokahuseracl::where('user_id',$request->id)->delete();

        foreach($request->acl as $acls){
            $acl = new Barokahuseracl();
            $acl->user_id = $user->id;
            $acl->access = $acls;
            $acl->save();
        }
        session()->flash('submission','success');

        return $this->userList();

    }

    public function login()
    {
        $page_title = 'Login';
        $page_description = '';

        $action = __FUNCTION__;

        return view('page.login', compact('page_title', 'page_description','action'));
    }

    public function loginPost(Request $request)
    {
        $user = Barokahuser::where('username',$request->username)->first();
        if($user) {
            if (Hash::check($request->password, $user->password)) {
                session()->push('user', $user);
                return redirect('/');
            }else{
                session()->flash('error','Wrong username/password!');
                return redirect()->route('login');
            }
        }else{
            session()->flash('error','Wrong username/password!');
            return redirect()->route('login');
        }
    }

    public function userDelete($id){
        $delete = Barokahuser::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->userList();
    }
}
