<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Barang;
use App\Models\Inventariskeluar;
use App\Models\Inventarismasuk;
use App\Models\Tukang;
use Carbon\Carbon;
use Cassandra\Date;
use Illuminate\Http\Request;

class InventarisController extends Controller
{
    function update_inventaris_masuk($id){
        $page_title = 'Update Barang #'.$id;
        $page_description = '';
        $isUpdate = true;
        $action = __FUNCTION__;
        $barangs = Barang::where('tipe','MATERIAL')->where('deleted_at',null)->get();
        $inventarismasuks = Inventarismasuk::find($id);
        return view('inventaris.masuk_form', compact('page_title', 'page_description','action','id','isUpdate','barangs','inventarismasuks'));
    }

    function new_inventaris_masuk(){
        $page_title = 'Tambah Barang Masuk';
        $page_description = '';
        $isUpdate = false;
        $action = __FUNCTION__;
        $barangs = Barang::where('tipe','MATERIAL')->where('deleted_at',null)->get();
        return view('inventaris.masuk_form', compact('page_title', 'page_description','action','isUpdate','barangs'));
    }

    function new_inventaris_keluar(){
        $page_title = 'Tambah Barang Keluar';
        $page_description = '';
        $isUpdate = false;
        $action = __FUNCTION__;
        $inventarismasuks = Inventarismasuk::groupBy('barang_id')->selectRaw("*, SUM(kuantitas) as total_kuantitas")->where('tipe','MATERIAL')->where('deleted_at',null)->get();
        $alamats = Alamat::where('deleted_at',null)->get();
        $tukangs = Tukang::where('deleted_at',null)->get();
        return view('inventaris.keluar_form', compact('page_title', 'page_description','action','isUpdate','inventarismasuks','alamats','tukangs'));
    }

    function update_inventaris_keluar($id){
        $page_title = 'Update Barang #'.$id;
        $page_description = '';
        $isUpdate = true;
        $action = __FUNCTION__;
        $inventarismasuks = Inventarismasuk::where('deleted_at',null)->get();
        $alamats = Alamat::where('deleted_at',null)->get();
        $tukangs = Tukang::where('deleted_at',null)->get();
        $inventariskeluars = Inventariskeluar::find($id);
        return view('inventaris.keluar_form', compact('page_title', 'page_description','action','id','isUpdate','inventarismasuks','alamats','tukangs','inventariskeluars'));
    }

    function stok(){
        $page_title = 'Ketersediaan Stok Barang';
        $page_description = '';

        $action = __FUNCTION__;

        $barangs = Barang::where('tipe','MATERIAL')->get();

        return view('inventaris.stok', compact('page_title', 'page_description','action','barangs'));
    }

    function inventarisOperasional()
    {
        $page_title = 'Inventaris Operasional';
        $page_description = '';

        $action = __FUNCTION__;

        $inventarismasuks = Inventarismasuk::OrderBy('id','desc')->where('tipe','OPERASIONAL')->where('deleted_at',null)->get();

        return view('inventaris.operasional', compact('page_title', 'page_description','action','inventarismasuks'));
    }

    function inventarisOperasionalNew()
    {
        $page_title = 'Tambah Barang Operasional';
        $page_description = '';
        $isUpdate = false;
        $action = __FUNCTION__;
        $barangs = Barang::where('tipe','OPERASIONAL')->get();
        return view('inventaris.operasional_form', compact('page_title', 'page_description','action','isUpdate','barangs'));
    }

    function inventarisOperasionalUpdate($id)
    {
        $page_title = 'Update Barang #'.$id;
        $page_description = '';
        $isUpdate = true;
        $action = __FUNCTION__;
        $inventarismasuks = Inventarismasuk::find($id);
        $barangs = Barang::where('tipe','OPERASIONAL')->get();
        return view('inventaris.operasional_form', compact('page_title', 'page_description','action','id','isUpdate','inventarismasuks','barangs'));
    }

    public function table_datatable_basic()
    {
        $page_title = 'Inventaris Masuk';
        $page_description = '';

        $action = __FUNCTION__;

        $inventarismasuks = Inventarismasuk::OrderBy('id','desc')->where('tipe','MATERIAL')->where('deleted_at',null)->get();

        return view('table.datatablebasic', compact('page_title', 'page_description','action','inventarismasuks'));
    }
    public function table_datatable_basic_2()
    {
        $page_title = 'Inventaris Keluar';
        $page_description = '';

        $action = __FUNCTION__;

        $inventariskeluars = Inventariskeluar::OrderBy('id','desc')->where('deleted_at',null)->get();

        return view('table.datatablebasic2', compact('page_title', 'page_description','action','inventariskeluars'));
    }

    public function inventarisMasukPost(Request $request)
    {
        $inventarismasuks = new Inventarismasuk();
        $inventarismasuks->barang_id = $request->barang_id;
        $inventarismasuks->tipe = "MATERIAL";
        $inventarismasuks->tanggal = $request->tanggal;
        $inventarismasuks->harga_satuan = numberOnly($request->harga_satuan);
        $inventarismasuks->kuantitas = numberOnly($request->kuantitas);
        $inventarismasuks->total = numberOnly($request->total);
        $inventarismasuks->save();

        session()->flash('submission','success');

        return $this->table_datatable_basic();
    }

    public function inventarisMasukUpdatePost(Request $request)
    {
        $inventarismasuks = Inventarismasuk::find($request->id);
        $inventarismasuks->barang_id = $request->barang_id;
        $inventarismasuks->tipe = "MATERIAL";
        $inventarismasuks->tanggal = $request->tanggal;
        $inventarismasuks->harga_satuan = numberOnly($request->harga_satuan);
        $inventarismasuks->kuantitas = numberOnly($request->kuantitas);
        $inventarismasuks->total = numberOnly($request->total);
        $inventarismasuks->save();

        session()->flash('submission','success');

        return $this->table_datatable_basic();
    }

    public function inventarisKeluarPost(Request $request)
    {
        $inventariskeluar = new Inventariskeluar();
        $inventariskeluar->barang_id = $request->barang_id;
        $inventariskeluar->alamat_id = $request->alamat_id;
        $inventariskeluar->user_id = $request->user_id;
        $inventariskeluar->tanggal = $request->tanggal;
        $inventariskeluar->jam = $request->jam;
        $inventariskeluar->status = $request->status;
        $inventariskeluar->harga_satuan = numberOnly($request->harga_satuan);
        $inventariskeluar->kuantitas = numberOnly($request->kuantitas);
        $inventariskeluar->total = numberOnly($request->total);
        $inventariskeluar->save();

        session()->flash('submission','success');

        return $this->table_datatable_basic_2();
    }

    public function inventarisKeluarUpdatePost(Request $request)
    {
        $inventariskeluar = Inventariskeluar::find($request->id);
        $inventariskeluar->barang_id = $request->barang_id;
        $inventariskeluar->alamat_id = $request->alamat_id;
        $inventariskeluar->user_id = $request->user_id;
        $inventariskeluar->tanggal = $request->tanggal;
        $inventariskeluar->jam = $request->jam;
        $inventariskeluar->status = $request->status;
        $inventariskeluar->harga_satuan = numberOnly($request->harga_satuan);
        $inventariskeluar->kuantitas = numberOnly($request->kuantitas);
        $inventariskeluar->total = numberOnly($request->total);
        $inventariskeluar->save();

        session()->flash('submission','success');

        return $this->table_datatable_basic_2();
    }

    public function inventarisOperasionalNewPost(Request $request)
    {
        $inventarismasuks = new Inventarismasuk();
        $inventarismasuks->barang_id = $request->barang_id;
        $inventarismasuks->tipe = "OPERASIONAL";
        $inventarismasuks->tanggal = $request->tanggal;
        $inventarismasuks->harga_satuan = numberOnly($request->harga_satuan);
        $inventarismasuks->kuantitas = numberOnly($request->kuantitas);
        $inventarismasuks->total = numberOnly($request->total);
        $inventarismasuks->save();

        session()->flash('submission','success');

        return $this->inventarisOperasional();
    }

    public function inventarisOperasionalUpdatePost(Request $request)
    {
        $inventarismasuks = Inventarismasuk::find($request->id);
        $inventarismasuks->barang_id = $request->barang_id;
        $inventarismasuks->tipe = "OPERASIONAL";
        $inventarismasuks->tanggal = $request->tanggal;
        $inventarismasuks->harga_satuan = numberOnly($request->harga_satuan);
        $inventarismasuks->kuantitas = numberOnly($request->kuantitas);
        $inventarismasuks->total = numberOnly($request->total);
        $inventarismasuks->save();

        session()->flash('submission','success');

        return $this->inventarisOperasional();
    }

    public function inventarisMasukDelete($id){
        $delete = Inventarismasuk::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->table_datatable_basic();
    }

    public function inventarisKeluarDelete($id){
        $delete = Inventariskeluar::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->table_datatable_basic_2();
    }

    public function inventarisOperasionalDelete($id){
        $delete = Inventarismasuk::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->inventarisOperasional();
    }
}
