<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Inventarismasuk;
use App\Models\Pembayaran;
use App\Models\Pembelian;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function getDataBarang(Request $request)
    {
        $barangs = Barang::where('kode_barang', $request->kode_barang)->first();
        return $data = ['barang_id' => $barangs->id, 'kodeBarang' => $barangs->kode_barang, 'namaBarang' => $barangs->nama_barang, 'satuan' => $barangs->satuan, 'hargaSatuan' => $barangs->harga_satuan];
    }

    public function getDataBarangMasuk(Request $request)
    {
        $inventarismasuks = Inventarismasuk::where('id', $request->id)->first();
        return $data = ['id'=>$inventarismasuks->id,'barang_id'=>$inventarismasuks->barang->id,'kodeBarang'=>$inventarismasuks->barang->kode_barang, 'namaBarang'=>$inventarismasuks->barang->nama_barang, 'satuan'=>$inventarismasuks->barang->satuan, 'hargaSatuan'=>$inventarismasuks->harga_satuan];
    }

    public function getPembeli(Request $request)
    {
        return $data = ['alamat'=>'Blok D3 Tahap 4'];
    }

    public function getDataBarangOperasional(Request $request)
    {
        $barangs = Barang::where('tipe','OPERASIONAL')->get();
    }

    public function getDataPembayaran (Request $request)
    {
        $pembelians = Pembelian::find($request->id);
        $total = Pembayaran::where('pembelian_id',$request->id)->where('jenis','cicilan')->sum('total_pembayaran');

        //get jumlah total bulan cicilan
        $totalbulan = numberOnly( $pembelians->pilihanpembayaran_id) * 12;

        $jumlahcicilan = $totalbulan -  Pembayaran::where('pembelian_id',$request->id)->where('jenis','cicilan')->count();

        $totalyangsudahdibayarkan = $pembelians->dp + $total;
        $sisacicilan = $pembelians->harga - $pembelians->dp - $total;
        return $data =[
            'nama'=>$pembelians->nama_pembeli,
            'nohp'=>$pembelians->nomor_handphone,
            'alamat'=>$pembelians->alamatpembelian->alamat,
            'totalyangsudahdibayarkan'=>numberWithCommas($totalyangsudahdibayarkan),
            'sisacicilan'=>numberWithCommas($sisacicilan),
            'sisajumlahcicilan'=>$jumlahcicilan
        ];
    }

    public function getDataPembeli(Request $request)
    {
        $pembelians = Pembelian::find($request->id);
        return $data = [
            'alamat'=>$pembelians->alamatpembelian->alamat
        ];
    }

    public function getDataPengembalian(Request $request)
    {
        $pembelians = Pembelian::find($request->id);
        $total = Pembayaran::where('pembelian_id',$request->id)->where('jenis','cicilan')->sum('total_pembayaran');
        $totalyangsudahdibayarkan = $pembelians->dp + $total;
        return $data = [
            'alamat'=>$pembelians->alamatpembelian->alamat,
            'tanggal_akad'=>$pembelians->tanggal_akad,
            'tanggal_pembayaran'=>$pembelians->tanggal_pembayaran,
            'total_pembayaran'=>$totalyangsudahdibayarkan
        ];
    }

}
