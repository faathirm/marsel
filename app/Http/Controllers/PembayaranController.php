<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use App\Models\Pembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PembayaranController extends Controller
{
    function pembayaranCicilan()
    {
        $page_title = 'Pembayaran';
        $page_description = '';

        $action = __FUNCTION__;

        $pembelians = Pembelian::where('deleted_at', null)->where('pilihanpembayaran_id','!=',0)->get();
        $isUpdate = false;

        return view('pembayaran.cicilan', compact('page_title', 'page_description', 'action', 'pembelians','isUpdate'));
    }

    function listPembayaran()
    {
        $page_title = 'Daftar Pembayaran';
        $page_description = '';

        $action = __FUNCTION__;
        $pembayaranscicilan = Pembayaran::where('jenis', 'cicilan')->where('deleted_at', null)->get();
        $pembayaransair = Pembayaran::where('jenis', 'air')->where('deleted_at', null)->get();

        return view('pembayaran.list', compact('page_title', 'page_description', 'action', 'pembayaransair', 'pembayaranscicilan'));
    }

    function pembayaranUpdate($id)
    {
        $page_title = 'Update Pembayaran';
        $page_description = '';
        $isUpdate = true;

        $action = __FUNCTION__;

        $pembelians = Pembelian::where('deleted_at', null)->get();
        $pembayarans = Pembayaran::find($id);
        return view('pembayaran.cicilan', compact('page_title', 'page_description', 'action', 'pembelians', 'isUpdate', 'pembayarans'));
    }

    function pembayaranNewPost(Request $request)
    {
        $pembayarans = new Pembayaran();
        $pembayarans->pembelian_id = $request->pembelian_id;
        $pembayarans->tanggal = $request->tanggal;
        $pembayarans->jenis = $request->jenis;
        $pembayarans->bulan = $request->bulan;
        if ($pembayarans->jenis == "cicilan")
            $pembayarans->total_pembayaran = numberOnly($request->total_pembayaran_cicilan);
        else {
            $pembayarans->total_pembayaran = numberOnly($request->total_pembayaran_air);
        }
        $pembayarans->save();

        session()->flash('submission', 'success');

        return $this->listPembayaran();
    }

    function pembayaranUpdatePost(Request $request)
    {
        $pembayarans = Pembayaran::find($request->pembayaran_id);
        $pembayarans->pembelian_id = $request->pembelian_id;
        $pembayarans->tanggal = $request->tanggal;
        $pembayarans->jenis = $request->jenis;
        $pembayarans->bulan = $request->bulan;
        if ($pembayarans->jenis == "cicilan")
            $pembayarans->total_pembayaran = numberOnly($request->total_pembayaran_cicilan);
        else {
            $pembayarans->total_pembayaran = numberOnly($request->total_pembayaran_air);
        }
        $pembayarans->save();

        session()->flash('submission', 'success');

        return $this->listPembayaran();
    }

    public function pembayaranDelete($id){
        $delete = Pembayaran::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->listPembayaran();
    }
}
