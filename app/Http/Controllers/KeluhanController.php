<?php

namespace App\Http\Controllers;

use App\Models\Keluhan;
use App\Models\Pembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KeluhanController extends Controller
{
    function keluhanList()
    {
        $page_title = 'Daftar Keluhan';
        $page_description = 'Some description for the page';

        $action = __FUNCTION__;

        $keluhans = Keluhan::where('deleted_at',null)->get();
        return view('keluhan.list', compact('page_title', 'page_description','action','keluhans'));
    }

    function keluhanTambah()
    {
        $page_title = 'Tambah Keluhan';
        $page_description = '';
        $isUpdate = false;
        $isView = false;
        $action = __FUNCTION__;
        $pembelians = Pembelian::where('deleted_at',null)->get();
        return view('keluhan.form', compact('page_title', 'page_description','action','isUpdate','isView','pembelians'));
    }

    function keluhanView($id)
    {
        $page_title = 'Lihat Keluhan';
        $page_description = '';
        $isUpdate = false;
        $isView = true;
        $action = __FUNCTION__;
        $pembelians = Pembelian::where('deleted_at',null)->get();
        $keluhans = Keluhan::find($id);
        return view('keluhan.form', compact('page_title', 'page_description','action','isUpdate','isView','pembelians','keluhans'));
    }

    function keluhanUpdate($id)
    {
        $page_title = 'Update Keluhan';
        $page_description = '';
        $isUpdate = true;
        $isView = false;
        $action = __FUNCTION__;
        $pembelians = Pembelian::where('deleted_at',null)->get();
        $keluhans = Keluhan::find($id);
        return view('keluhan.form', compact('page_title', 'page_description','action','isUpdate','isView','pembelians','keluhans'));
    }

    public function keluhanPostNew(Request $request)
    {
        $keluhan = new Keluhan();
        $keluhan->pembelian_id = $request->pembelian_id;
        $keluhan->tanggal = $request->tanggal;
        $keluhan->keluhan = $request->keluhan;
        $keluhan->kebutuhan = $request->kebutuhan;
        $keluhan->status = $request->status;
        $keluhan->save();

        session()->flash('submission','success');

        return $this->keluhanList();
    }

    public function keluhanPostUpdate(Request $request)
    {
        $keluhan = Keluhan::find($request->id);
        $keluhan->pembelian_id = $request->pembelian_id;
        $keluhan->tanggal = $request->tanggal;
        $keluhan->keluhan = $request->keluhan;
        $keluhan->kebutuhan = $request->kebutuhan;
        $keluhan->status = $request->status;
        $keluhan->save();

        session()->flash('submission','success');

        return $this->keluhanList();
    }

    public function keluhanDelete($id){
        $delete = Keluhan::find($id);
        $delete->deleted_at = Carbon::now();
        $delete->save();

        return $this->keluhanList();
    }

}
