<?php

Function numberWithCommas($number){
    return number_format($number,0, ',' , '.');
}

function numberOnly($number){
    return preg_replace("/[^0-9]/", "",$number);
}

function checkACL($aclName)
{
    $id = Session()->get('user')[0]["id"];
    $acl = \App\Models\Barokahuseracl::where('user_id',$id)->where('access',$aclName)->exists();
    return $acl;
}

function checkTotalKuantitas($kode_barang)
{
    $masuk = \App\Models\Inventarismasuk::where('barang_id',$kode_barang)->sum('kuantitas');
    $keluar = \App\Models\Inventariskeluar::where('barang_id',$kode_barang)->sum('kuantitas');
    return $masuk - $keluar;
}

function getParameter($parameter)
{
    $parameters = \App\Models\Parameter::where('deleted_at',null)->where('parameter',$parameter)->first();
    return $parameters->value;
}
