<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    use HasFactory;

    public function alamatpembelian()
    {
        return $this->hasOne(Alamat::class, 'id', 'alamat_id');
    }

    public function pilihan_pembayaran()
    {
        return $this->hasOne(Pilihanpembayaran::class, 'id', 'pilihanpembayaran_id');
    }

}
