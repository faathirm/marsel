<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventariskeluar extends Model
{
    use HasFactory;

    public function barang()
    {
        return $this->hasOne(Barang::class, 'id', 'barang_id');
    }

    public function alamat()
    {
        return $this->hasOne(Alamat::class, 'id', 'alamat_id');
    }

    public function tukang()
    {
        return $this->hasOne(Tukang::class, 'id', 'user_id');
    }
}
