<div class="deznav">
            <div class="deznav-scroll">
				<ul class="metismenu" id="menu">
                    @if(checkACL('dashboard-view'))<li><a href="{!! url('/dashboard'); !!}" class="ai-icon" aria-expanded="false">
							<i class="mdi mdi-chart-line"></i>
							<span class="nav-text">Dashboard</span>
						</a>
                    </li>
                    @endif
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-view-list"></i>
                            <span class="nav-text">Inventaris</span>
                        </a>
                        <ul aria-expanded="false">
                            @if(checkACL('inventaris-stok-view'))<li class="text-left"><a href="{!! url('/inventaris/stok'); !!}">Ketersediaan Stok Barang</a></li>@endif
                            @if(checkACL('inventaris-masuk-view'))<li class="text-left"><a href="{!! url('/inventaris/masuk'); !!}">Inventaris Masuk</a></li>@endif
                            @if(checkACL('inventaris-keluar-view'))<li class="text-left"><a href="{!! url('/inventaris/keluar'); !!}">Inventaris Keluar</a></li>@endif
                            @if(checkACL('inventaris-operasional-view')) <li class="text-left"><a href="{!! url('/inventaris/operasional'); !!}">Inventaris Operasional</a></li>@endif
                        </ul>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="mdi mdi-clipboard-account"></i>
                            <span class="nav-text">Pembeli</span>
                        </a>
                        <ul aria-expanded="false">
                            @if(checkACL('pembeli-pembelian-view'))<li class="text-left"><a href="{!! url('/pembeli/pembelian'); !!}">Pembelian</a></li>@endif
                            @if(checkACL('pembeli-pengembalian-view'))<li class="text-left"><a href="{!! url('/pembeli/pengembalian'); !!}">Pengembalian</a></li>@endif
                        </ul>
                    </li>
                    @if(checkACL('pembayaran-view'))
                    <li><a href="{!! url('/pembayaran'); !!}" class="ai-icon" aria-expanded="false">
                            <i class="mdi mdi-credit-card"></i>
                            <span class="nav-text">Pembayaran</span>
                        </a>
                    </li>
                    @endif
                    @if(checkACL('penggajian-karyawan-view') || checkACL('penggajian-tukang-view'))
                    <li><a href="{!! url('/penggajian'); !!}" class="ai-icon" aria-expanded="false">
                            <i class="mdi mdi-cash"></i>
                            <span class="nav-text">Penggajian</span>
                        </a>
                    </li>
                    @endif
                    @if(checkACL('keluhan-view'))
                    <li><a href="{!! url('/keluhan'); !!}" class="ai-icon" aria-expanded="false">
                            <i class="mdi mdi-alert-outline"></i>
                            <span class="nav-text">Laporan Keluhan</span>
                        </a>
                    </li>
                    @endif
                    @if(checkACL('pengguna-view'))
                    <li><a href="{!! url('/user'); !!}" class="ai-icon" aria-expanded="false">
                            <i class="mdi mdi-account-group"></i>
                            <span class="nav-text">Kelola Pengguna</span>
                        </a>
                    </li>
                    @endif
                </ul>
			</div>
        </div>
