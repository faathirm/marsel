{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Inventaris</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Inventaris</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Operasional</a></li>
                    </ol>
                </div>
            </div>
            <div>
                <a href="{{route('inventarisOperasionalNew')}}" type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i> Tambah Barang Operasional</a>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th>Satuan</th>
                                    <th>Kuantitas</th>
                                    <th>Harga Satuan</th>
                                    <th>Total Pembelian</th>
                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($inventarismasuks as $inventarismasuk)
                                <tr>
                                    <td>{{$x++}}</td>
                                    <td>{{$inventarismasuk->tanggal}}</td>
                                    <td>{{$inventarismasuk->barang->nama_barang}}</td>
                                    <td>{{$inventarismasuk->barang->satuan}}</td>
                                    <td>{{numberWithCommas($inventarismasuk->kuantitas)}}</td>
                                    <td>Rp {{numberWithCommas($inventarismasuk->harga_satuan)}}</td>
                                    <td>Rp {{numberWithCommas($inventarismasuk->total)}}</td>
                                    <td>
                                        <div class="btn-group ">
                                            @if(checkACL('inventaris-operasional-update'))<a type="button" href="{{route('inventarisOperasionalUpdate',$inventarismasuk->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                            @if(checkACL('inventaris-operasional-delete'))<a type="button" href="{{route('inventarisOperasionalDelete',$inventarismasuk->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="bg-primary-light">
                                    <th colspan="4" class="text-left">Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr class="bg-primary">
                                    <th colspan="6" class="text-left text-white">Grand Total</th>
                                    <th class="grandTotal text-white"></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
