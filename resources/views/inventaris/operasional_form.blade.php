{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate == true)
                    <h2 class="text-black font-w600">Update Barang Masuk</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Inventaris</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('inventarisMasuk')}}">Operasional</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$inventarismasuks->id}}</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Tambah Barang Operasional</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Inventaris</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('inventarisOperasional')}}">Operasional</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah Barang Operasional</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" @if($isUpdate == true) action="{{route('inventarisOperasionalUpdatePost',$inventarismasuks->id)}}" @else action="{{route('inventarisOperasionalNewPost')}}" @endif>
                                @csrf
                                @if($isUpdate == true) <input type="hidden" name="id" value="{{$inventarismasuks->id}}"> @endif
                                <input type="hidden" class="inventarisMasukIdBarang" name="barang_id" @if($isUpdate == true) value="{{$inventarismasuks->barang_id}}" @endif>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Barang Masuk</label>
                                    <div class="col-sm-9">
                                        <input name="tanggal" class="datepicker-default form-control" id="datepicker" @if($isUpdate == true) value="{{$inventarismasuks->tanggal}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kode Barang</label>
                                    <div class="col-sm-9">
                                        <button type="button" class="btn btn-outline-light btn-block text-left buttonDropdownKodeBarang dropdown-toggle" data-toggle="dropdown">
                                            @if($isUpdate == true) {{$inventarismasuks->barang->kode_barang}} @else Pilih Kode Barang @endif
                                        </button>
                                        <div class="dropdown-menu w-75">
                                            <input data-value="*" type="text" class="border-light form-control searchKodeBarang" placeholder="Cari kode / nama barang...">
                                            @foreach($barangs as $barang)
                                                <a class="dropdown-item dropdownKodeBarang" data-value="{{$barang->kode_barang}}" href="#" >[{{$barang->kode_barang}}] {{$barang->nama_barang}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Barang</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formNamaBarang bg-light" @if($isUpdate == true) value="{{$inventarismasuks->barang->nama_barang}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Satuan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formSatuanBarang bg-light" @if($isUpdate == true) value="{{$inventarismasuks->barang->satuan}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Harga Satuan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formHargaBarang bg-light" name="harga_satuan" @if($isUpdate == true) value="{{$inventarismasuks->harga_satuan}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kuantitas</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control inputKuantitas" name="kuantitas" @if($isUpdate == true) value="{{$inventarismasuks->kuantitas}}" @else value="0" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Pembelian</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light totalPembelian" name="total" @if($isUpdate == true) value="Rp. {{numberWithCommas($inventarismasuks->total)}}" @else value="Rp. 0" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
