{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate == true)
                    <h2 class="text-black font-w600">Update Barang Keluar</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Inventaris</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('inventarisKeluar')}}">Keluar</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#12312</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Tambah Barang Keluar</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Inventaris</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('inventarisKeluar')}}">Keluar</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah Barang Keluar</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" @if($isUpdate) action="{{route('inventarisKeluarUpdatePost')}}" @else action="{{route('inventarisKeluarPost')}}" @endif>
                                @csrf
                                @if($isUpdate) <input type="hidden" name="id" value="{{$inventariskeluars->id}}"> @endif
                                <input type="hidden" name="barang_id" class="barang_id" @if($isUpdate == true) value="{{$inventariskeluars->barang_id}}" @endif>
                                <input type="hidden" name="alamat_id" class="alamat_id" @if($isUpdate == true) value="{{$inventariskeluars->alamat_id}}" @endif>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Barang Keluar</label>
                                    <div class="col-sm-9">
                                        <input name="tanggal" class="datepicker-default form-control" id="datepicker" @if($isUpdate == true) value="{{$inventariskeluars->tanggal}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jam Barang Keluar</label>
                                    <div class="input-group col-sm-9 clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
                                        <input type="text" class="form-control" name="jam" @if($isUpdate == true) value="{{$inventariskeluars->jam}}" @endif>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kode Barang</label>
                                    <div class="col-sm-9">
                                        <button type="button" class="btn btn-outline-light btn-block text-left buttonDropdownKodeBarang dropdown-toggle" data-toggle="dropdown">
                                            @if($isUpdate == true) {{$inventariskeluars->barang->kode_barang}} @else Pilih Kode Barang @endif
                                        </button>
                                        <div class="dropdown-menu w-75">
                                            <input data-value="*" type="text" class="border-light form-control searchKodeBarangKeluar" placeholder="Cari kode / nama barang...">
                                            @foreach($inventarismasuks  as $inventarismasuk)
                                                <a class="dropdown-item dropdownKodeBarangKeluar" data-value="{{$inventarismasuk->id}}" href="#" >[{{$inventarismasuk->barang->kode_barang}}] {{$inventarismasuk->barang->nama_barang}} <i class="pull-right text-primary">{{checkTotalKuantitas($inventarismasuk->barang_id)}}</i></a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Barang</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formNamaBarang bg-light" @if($isUpdate == true) value="{{$inventariskeluars->barang->nama_barang}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Harga Satuan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formHargaSatuanBarang bg-light" name="harga_satuan" @if($isUpdate == true) value="{{$inventariskeluars->harga_satuan}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Satuan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formSatuanBarang bg-light" @if($isUpdate == true) value="{{$inventariskeluars->barang->satuan}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <button type="button" class="btn btn-outline-light btn-block text-left buttonDropdownAlamat dropdown-toggle" data-toggle="dropdown">
                                            @if($isUpdate == true) {{$inventariskeluars->alamat->alamat}} @else Pilih Alamat @endif
                                        </button>
                                        <div class="dropdown-menu w-75">
                                            <input data-value="*" type="text" class="border-light form-control searchAlamatKeluar"  placeholder="Cari alamat...">
                                            @foreach($alamats  as $alamat)
                                                <a class="dropdown-item dropdownAlamat" data-value="{{$alamat->id}}" href="#" >{{$alamat->alamat}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">User</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="user_id">
                                            @foreach($tukangs as $tukang)
                                                <option value="{{$tukang->id}}" @if($isUpdate == true && $tukang->id == $inventariskeluars->user_id) selected @endif>{{$tukang->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="status">
                                            <option {{$isUpdate?'selected':''}} value="Barang baru">Barang baru</option>
                                            <option {{$isUpdate?'selected':''}} value="Perbaikan">Perbaikan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kuantitas</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control kuantitasBarangKeluar" @if($isUpdate == true) value="{{numberWithCommas($inventariskeluars->kuantitas)}}" @else value="0" @endif name="kuantitas">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Pengeluaran</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light totalPengeluaran" name="total" @if($isUpdate == true) value="Rp. {{numberWithCommas($inventariskeluars->total)}}" @else value="0" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
