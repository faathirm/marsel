{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Stok</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Ketersediaan Stok Barang</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example4" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Satuan Barang</th>
                                    <th>Jumlah Stok Barang</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($barangs as $barang)
                                @php
                                $barangMasuk = \App\Models\Inventarismasuk::where('barang_id',$barang->id)->sum("kuantitas");
                                $barangKeluar = \App\Models\Inventariskeluar::where('barang_id',$barang->id)->sum("kuantitas");
                                @endphp
                                <tr>
                                        <td>{{$x++}}</td>
                                        <td>{{$barang->kode_barang}}</td>
                                        <td>{{$barang->nama_barang}}</td>
                                        <td>{{$barang->satuan}}</td>
                                        <td>{{$barangMasuk-$barangKeluar}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
