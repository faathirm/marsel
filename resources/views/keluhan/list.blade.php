{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Keluhan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Keluhan</a></li>
                    </ol>
                </div>
            </div>
            @if(checkACL('keluhan-add'))
            <div>
                <a href="{{route('keluhanTambah')}}" type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i> Keluhan Baru</a>
            </div>
            @endif
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example7" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Pemilik Rumah</th>
                                    <th>Alamat</th>
                                    <th>Status</th>
                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($keluhans as $keluhan)
                                <tr>
                                    <td>{{$x++}}</td>
                                    <td>{{$keluhan->tanggal}}</td>
                                    <td>{{$keluhan->pembelian->nama_pembeli}}</td>
                                    <td>{{$keluhan->pembelian->alamatpembelian->alamat}}</td>
                                    <td>{{$keluhan->status}}</td>
                                    <td>
                                        <div class="btn-group ">
                                            @if(checkACL('keluhan-view'))<a type="button" href="{{route('keluhanView',$keluhan->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-eye mdi-18px"></i></a>@endif
                                            @if(checkACL('keluhan-update'))<a type="button" href="{{route('keluhanUpdate',$keluhan->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                            @if(checkACL('keluhan-delete'))<a type="button" href="{{route('keluhanDelete',$keluhan->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
