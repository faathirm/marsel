{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate == true)
                    <h2 class="text-black font-w600">Update Keluhan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="{{route('keluhanList')}}">Keluhan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#12312</a></li>
                    </ol>
                @elseif($isView)
                    <h2 class="text-black font-w600">Lihat Keluhan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="{{route('keluhanList')}}">Keluhan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$keluhans->id}}</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Keluhan Baru</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="{{route('keluhanList')}}">Keluhan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Keluhan Baru</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            <input type="hidden" name="kodeBarang" class="kodeBarangHidden">
                        </form>
                        <div class="basic-form">
                            <form method="POST" @if($isUpdate) action="{{route('keluhanPostUpdate')}}" @else action="{{route('keluhanPostNew')}}" @endif>
                                @csrf
                                @if($isUpdate) <input type="hidden" name="id" value="{{$keluhans->id}}"> @endif
                                <input type="hidden" name="pembelian_id" class="keluhanPembelianId" @if($isUpdate) value="{{$keluhans->pembelian_id}}" @endif>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Keluhan</label>
                                    <div class="col-sm-9">
                                        @if($isView)
                                            <p class="text-primary">{{$keluhans->tanggal}}</p>
                                        @else
                                        <input class="datepicker-default form-control" id="datepicker" name="tanggal" @if($isUpdate) value="{{$keluhans->tanggal}}" @endif>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pemilik Rumah</label>
                                    <div class="col-sm-9">
                                        @if($isView)
                                            <p class="text-primary">{{$keluhans->pembelian->nama_pembeli}}</p>
                                        @else
                                        <button type="button" class="btn btn-outline-light btn-block text-left keluhanButtonDropdown dropdown-toggle" data-toggle="dropdown">
                                            @if($isUpdate) {{$keluhans->pembelian->nama_pembeli}} @else Pilih nama pemilik rumah @endif
                                        </button>
                                        <div class="dropdown-menu w-75">

                                            <input data-value="*" type="text" class="border-light form-control keluhanSearchNamaPembeli" placeholder="Cari nama pembeli...">
                                            @foreach($pembelians as $pembelian)
                                                <a class="dropdown-item dropdownNamaPembeli" data-value="{{$pembelian->id}}" href="#" >{{$pembelian->nama_pembeli}}</a>
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control keluhanAlamat bg-light" @if($isView || $isUpdate) value="{{$keluhans->pembelian->alamatpembelian->alamat}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Keluhan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control {{$isView==true?'bg-light':''}}" {{$isView==true?'readonly':''}} rows="4" id="comment" name="keluhan">@if($isView || $isUpdate) {{$keluhans->keluhan}} @endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Kebutuhan Barang</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control {{$isView==true?'bg-light':''}}" {{$isView==true?'readonly':''}} rows="4" id="comment" name="kebutuhan">@if($isView || $isUpdate) {{$keluhans->kebutuhan}} @endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3 col-form-label">Status</div>
                                    <div class="col-sm-9">
                                        <select class="form-control {{$isView==true?'bg-light':''}}" {{$isView==true?'disabled':''}} name="status">
                                            <option {{$isView || $isUpdate && $keluhans->status=='Pending'?'selected':''}} value="Pending">Pending</option>
                                            <option {{$isView || $isUpdate && $keluhans->status=='Done'?'selected':''}} value="Done">Done</option>
                                        </select>
                                    </div>
                                </div>
                                @if(!$isView)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
