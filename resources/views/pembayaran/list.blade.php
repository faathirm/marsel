{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Pembayaran</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pembayaran</a></li>
                    </ol>
                </div>
            </div>
            <div>
                <a href="{{route('pembayaranCicilan')}}" type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i> Pembayaran Baru</a>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Pembayaran Cicilan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example6" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Pemilik Rumah</th>
                                    <th>Pembayaran Bulan</th>
                                    <th>Total Pembayaran</th>

                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($pembayaranscicilan as $pembayaran)
                                <tr>
                                    <td>{{$x++}}</td>
                                    <td>{{$pembayaran->tanggal}}</td>
                                    <td>{{$pembayaran->pembelian->nama_pembeli}} - {{$pembayaran->pembelian->alamatpembelian->alamat}}</td>
                                    <td>{{$pembayaran->bulan}}</td>
                                    <td>Rp. {{numberWithCommas($pembayaran->total_pembayaran)}}</td>
                                    <td>
                                        <div class="btn-group ">
                                            @if(checkACL("pembayaran-view"))<button type="button" class="btn btn-outline-dark"><i class="mdi mdi-printer mdi-18px"></i></button>@endif
                                            @if(checkACL("pembayaran-update"))<a type="button" href="{{route('pembayaranUpdate',$pembayaran->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                            @if(checkACL("pembayaran-delete"))<a type="button" href="{{route('pembayaranDelete',$pembayaran->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="bg-primary-light">
                                    <th colspan="4" class="text-left">Total</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr class="bg-primary">
                                    <th colspan="4" class="text-left text-white">Grand Total</th>
                                    <th class="grandTotalCicilan text-white"></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Pembayaran Air
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example5" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Pemilik Rumah</th>
                                    <th>Pembayaran Bulan</th>
                                    <th>Total Pembayaran</th>

                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($pembayaransair as $pembayaran)
                                    <tr>
                                        <td>{{$x++}}</td>
                                        <td>{{$pembayaran->tanggal}}</td>
                                        <td>{{$pembayaran->pembelian->nama_pembeli}} - {{$pembayaran->pembelian->alamatpembelian->alamat}}</td>
                                        <td>{{$pembayaran->bulan}}</td>
                                        <td>Rp. {{numberWithCommas($pembayaran->total_pembayaran)}}</td>
                                        <td>
                                            <div class="btn-group ">
                                                @if(checkACL("pembayaran-view"))<button type="button" class="btn btn-outline-dark"><i class="mdi mdi-printer mdi-18px"></i></button>@endif
                                                @if(checkACL("pembayaran-update"))<a type="button" href="{{route('pembayaranUpdate',$pembayaran->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                                @if(checkACL("pembayaran-delete"))<a type="button" href="{{route('pembayaranDelete',$pembayaran->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="bg-primary-light">
                                    <th colspan="4" class="text-left">Total</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr class="bg-primary">
                                    <th colspan="4" class="text-left text-white">Grand Total</th>
                                    <th class="grandTotalAir text-white"></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
