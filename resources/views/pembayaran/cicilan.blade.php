{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                <h2 class="text-black font-w600">Pembayaran</h2>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="{{route('pembayaranList')}}">Pembayaran</a></li>
                    @if($isUpdate)
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pembayaran #{{$pembayarans->id}}</a></li>
                    @else
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pembayaran Baru</a></li>
                    @endif
                </ol>
            </div>
        </div>
        <!-- row -->
        <form method="POST" @if($isUpdate) action="{{route('pembayaranUpdatePost')}}" @else action="{{route('pembayaranNewPost')}}" @endif>
            @csrf
            @if($isUpdate) <input type="hidden" name="pembayaran_id" value="{{$pembayarans->id}}"> @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Data Pembayaran
                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pembeli</label>
                                    <div class="col-sm-9">
                                        <select class="form-control pembayaranPembeli" name="pembelian_id">
                                            @foreach($pembelians as $pembelian)
                                                <option @if($isUpdate && $pembayarans->pembeli_id == $pembelian->id ) selected @endif value="{{$pembelian->id}}">{{$pembelian->nama_pembeli}} - {{$pembelian->alamatpembelian->alamat}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-9">
                                        <input name="tanggal" class="datepicker-default form-control" id="datepicker" @if($isUpdate) value="{{$pembayarans->tanggal}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis Pembayaran</label>
                                    <div class="col-sm-9">
                                        <select class="form-control pembayaranJenisPembayaran" name="jenis">
                                            <option @if($isUpdate && $pembayarans->jenis == "air") selected @endif value="air">Air</option>
                                            <option @if($isUpdate && $pembayarans->jenis == "cicilan") selected @endif value="cicilan">Cicilan Rumah</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pembayaran Bulan</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="bulan">
                                            <option @if($isUpdate && $pembayarans->bulan == "Januari") selected @endif value="Januari">Januari</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Februari") selected @endif value="Februari">Februari</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Maret") selected @endif value="Maret">Maret</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "April") selected @endif value="April">April</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Mei") selected @endif value="Mei">Mei</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Juni") selected @endif value="Juni">Juni</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Juli") selected @endif value="Juli">Juli</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Agustus") selected @endif value="Agustus">Agustus</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "September") selected @endif value="September">September</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Oktober") selected @endif value="Oktober">Oktober</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "November") selected @endif value="November">November</option>
                                            <option @if($isUpdate && $pembayarans->bulan == "Desember") selected @endif value="Desember">Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary viewVerifikasi">Verifikasi</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6  displayVerifikasi" style="display: none;">
                    <div class="card">
                        <div class="card-header">
                            Verifikasi Data
                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pemilik Rumah</label>
                                    <div class="col-sm-9">
                                        <p class="text-primary pembayaranNama"></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No HP</label>
                                    <div class="col-sm-9">
                                        <p class="text-primary pembayaranNomor "></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat Rumah</label>
                                    <div class="col-sm-9">
                                        <p class="text-primary pembayaranAlamat"></p>
                                    </div>
                                </div>
                                <div class="form-group row pembayaranCicilan">
                                    <label class="col-sm-3 col-form-label">Total Pembayaran Cicilan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formRupiah pembayaranTotalCicilan" name="total_pembayaran_cicilan" @if($isUpdate) value="Rp. {{numberWithCommas($pembayarans->total_pembayaran)}}" @else value="Rp. 0" @endif>
                                    </div>
                                </div>
                                <div class="form-group row pembayaranCicilan">
                                    <label class="col-sm-3 col-form-label">Total Yang Sudah Dibayarkan</label>
                                    <div class="col-sm-9">
                                        <p class="text-primary pembayaranTotalYangSudahDibayarkan">Rp. 0</p>
                                    </div>
                                </div>
                                <div class="form-group row pembayaranCicilan">
                                    <label class="col-sm-3 col-form-label">Sisa Cicilan</label>
                                    <div class="col-sm-9">
                                        <p class="text-primary pembayaranSisa">0</p>
                                    </div>
                                </div>
                                <div class="form-group row pembayaranCicilan">
                                    <label class="col-sm-3 col-form-label">Sisa Yang Harus Dibayarkan</label>
                                    <div class="col-sm-9">
                                        <p class="text-primary pembayaranSisaYangSudahDibayarkan">Rp. 0</p>
                                    </div>
                                </div>
                                <div class="form-group row pembayaranAir">
                                    <label class="col-sm-3 col-form-label">Total Pembayaran</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control formRupiah pembayaranTotalAir"  name="total_pembayaran_air" @if($isUpdate) value="Rp. {{numberWithCommas($pembayarans->total_pembayaran)}}" @else value="Rp. 0" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
