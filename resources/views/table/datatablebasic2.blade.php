{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Inventaris</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Inventaris</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Keluar</a></li>
                    </ol>
                </div>
            </div>
            <div>
                @if(checkACL('inventaris-keluar-add')) <a href="{{route('inventarisTambahKeluar')}}" type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i> Tambah Barang Keluar</a> @endif
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive table-">
                            <table id="example2" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th>Satuan</th>
                                    <th>Alamat</th>
                                    <th>User</th>
                                    <th>Status</th>
                                    <th>Kuantitas</th>
                                    <th>Harga Satuan</th>
                                    <th>Total Pengeluaran</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($inventariskeluars as $inventariskeluar)
                                <tr>
                                        <td>{{$x++}}</td>
                                        <td>{{$inventariskeluar->tanggal}} - {{$inventariskeluar->jam}}</td>
                                        <td>{{$inventariskeluar->barang->nama_barang}}</td>
                                        <td>{{$inventariskeluar->barang->satuan}}</td>
                                        <td>{{$inventariskeluar->alamat->alamat}}</td>
                                        <td>{{$inventariskeluar->tukang->nama}}</td>
                                        <td>{{$inventariskeluar->status}}</td>
                                        <td>{{numberWithCommas($inventariskeluar->kuantitas)}}</td>
                                        <td>Rp {{numberWithCommas($inventariskeluar->harga_satuan)}}</td>
                                        <td>Rp {{numberWithCommas($inventariskeluar->total)}}</td>
                                        <td>
                                            <div class="btn-group ">
                                                @if(checkACL('inventaris-keluar-update'))<a type="button" href="{{route('inventarisUpdateKeluar',$inventariskeluar->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                                @if(checkACL('inventaris-keluar-delete'))<a type="button" href="{{route('inventarisKeluarDelete',$inventariskeluar->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                            </div>
                                        </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="bg-primary-light">
                                    <th colspan="7" class="text-left">Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr class="bg-primary">
                                    <th colspan="9" class="text-left text-white">Grand Total</th>
                                    <th class="grandTotal text-white"></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
