{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate == true)
                    <h2 class="text-black font-w600">Update Pengguna</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="{{route('userList')}}">Kelola Pengguna</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$user->id}}</a></li>
                    </ol>
                @elseif($isView == true)
                    <h2 class="text-black font-w600">Lihat Pengguna</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="{{route('userList')}}">Kelola Pengguna</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$user->id}}</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Tambah Pengguna</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="{{route('userList')}}">Kelola Pengguna</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah Pengguna</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            <input type="hidden" name="kodeBarang" class="kodeBarangHidden">
                        </form>
                        <div class="basic-form">
                            <form method="POST" @if($isUpdate) action="{{route('userPostUpdate')}}" @else action="{{route('userPostNew')}}" @endif>
                                @csrf
                                @if($isUpdate) <input type="hidden" name="id" value="{{$user->id}}"> @endif
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Username</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control @if($isView) bg-light @endif" name="username" @if($isView) value="{{$user->username}}" readonly @elseif($isUpdate) value="{{$user->username}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control @if($isView) bg-light @endif" name="nama" @if($isView) value="{{$user->nama}}" readonly @elseif($isUpdate) value="{{$user->nama}}" @endif>
                                    </div>
                                </div>
                                @if($isView == false)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="password">
                                        @if($isUpdate) <i class="small">Biarkan kosong jika tidak ingin mengubah password.</i> @endif
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Otoritas</label>
                                    <div class="col-sm-9">
                                        <p class="m-0 p-0 font-weight-bold">Dashboard</p>
                                        <div class="form-check mb-2">
                                            <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','dashboard-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','dashboard-view') ? 'checked':''}} @endif value="dashboard-view">
                                            <label class="form-check-label" for="check1">View</label>
                                        </div>

                                        <p class="m-0 p-0 font-weight-bold">Inventaris</p>
                                        <p class="m-0 p-0 ml-4">Ketersediaan Stok Barang</p>
                                        <div class="form-check mb-2 ml-4">
                                            <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-stok-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-stok-view') ? 'checked':''}} @endif  value="inventaris-stok-view">
                                            <label class="form-check-label" for="check1">View</label>
                                        </div>
                                        <p class="m-0 p-0 ml-4">Inventaris Masuk</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-masuk-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-masuk-view') ? 'checked':''}} @endif  value="inventaris-masuk-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-masuk-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-masuk-add') ? 'checked':''}} @endif  value="inventaris-masuk-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-masuk-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-masuk-update') ? 'checked':''}} @endif  value="inventaris-masuk-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-masuk-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-masuk-delete') ? 'checked':''}} @endif  value="inventaris-masuk-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 ml-4">Inventaris Keluar</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]"  @if($isView) disabled {{$acl->contains('access','inventaris-keluar-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-keluar-view') ? 'checked':''}} @endif  value="inventaris-keluar-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-keluar-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-keluar-add') ? 'checked':''}} @endif  value="inventaris-keluar-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-keluar-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-keluar-update') ? 'checked':''}} @endif  value="inventaris-keluar-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-keluar-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-keluar-delete') ? 'checked':''}} @endif  value="inventaris-keluar-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 ml-4">Inventaris Operasional</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-operasional-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-operasional-view') ? 'checked':''}} @endif  value="inventaris-operasional-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-operasional-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-operasional-add') ? 'checked':''}} @endif  value="inventaris-operasional-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-operasional-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-operasional-update') ? 'checked':''}} @endif  value="inventaris-operasional-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','inventaris-operasional-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','inventaris-operasional-delete') ? 'checked':''}} @endif  value="inventaris-operasional-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 font-weight-bold">Pembeli</p>
                                        <p class="m-0 p-0 ml-4">Data Pembeli</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pembelian-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pembelian-view') ? 'checked':''}} @endif  value="pembeli-pembelian-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pembelian-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pembelian-add') ? 'checked':''}} @endif  value="pembeli-pembelian-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pembelian-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pembelian-update') ? 'checked':''}} @endif  value="pembeli-pembelian-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pembelian-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pembelian-delete') ? 'checked':''}} @endif  value="pembeli-pembelian-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 ml-4">Data Pengembalian</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pengembalian-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pengembalian-view') ? 'checked':''}} @endif  value="pembeli-pengembalian-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pengembalian-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pengembalian-add') ? 'checked':''}} @endif  value="pembeli-pengembalian-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pengembalian-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pengembalian-update') ? 'checked':''}} @endif  value="pembeli-pengembalian-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembeli-pengembalian-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembeli-pengembalian-delete') ? 'checked':''}} @endif  value="pembeli-pengembalian-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 font-weight-bold">Pembayaran</p>
                                        <div class="form-check mb-2">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','pembayaran-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembayaran-view') ? 'checked':''}} @endif  value="pembayaran-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembayaran-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembayaran-add') ? 'checked':''}} @endif  value="pembayaran-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembayaran-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembayaran-update') ? 'checked':''}} @endif  value="pembayaran-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pembayaran-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pembayaran-delete') ? 'checked':''}} @endif  value="pembayaran-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 font-weight-bold">Penggajian</p>
                                        <p class="m-0 p-0 ml-4">Karyawan</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-karyawan-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-karyawan-view') ? 'checked':''}} @endif  value="penggajian-karyawan-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-karyawan-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-karyawan-add') ? 'checked':''}} @endif  value="penggajian-karyawan-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-karyawan-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-karyawan-update') ? 'checked':''}} @endif  value="penggajian-karyawan-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-karyawan-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-karyawan-delete') ? 'checked':''}} @endif  value="penggajian-karyawan-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 ml-4">Tukang</p>
                                        <div class="form-check mb-2 ml-4">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-tukang-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-tukang-view') ? 'checked':''}} @endif  value="penggajian-tukang-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-tukang-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-tukang-add') ? 'checked':''}} @endif  value="penggajian-tukang-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-tukang-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-tukang-update') ? 'checked':''}} @endif  value="penggajian-tukang-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','penggajian-tukang-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','penggajian-tukang-delete') ? 'checked':''}} @endif  value="penggajian-tukang-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 font-weight-bold">Keluhan</p>
                                        <div class="form-check mb-2">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','keluhan-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','keluhan-view') ? 'checked':''}} @endif  value="keluhan-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','keluhan-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','keluhan-add') ? 'checked':''}} @endif  value="keluhan-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','keluhan-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','keluhan-update') ? 'checked':''}} @endif  value="keluhan-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','keluhan-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','keluhan-delete') ? 'checked':''}} @endif  value="keluhan-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="m-0 p-0 font-weight-bold">Kelola Pengguna</p>
                                        <div class="form-check mb-2">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input" name="acl[]" @if($isView) disabled {{$acl->contains('access','pengguna-view') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pengguna-view') ? 'checked':''}} @endif  value="pengguna-view">
                                                    <label class="form-check-label" for="check1">View</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pengguna-add') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pengguna-add') ? 'checked':''}} @endif  value="pengguna-add">
                                                    <label class="form-check-label" for="check1">Add</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pengguna-update') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pengguna-update') ? 'checked':''}} @endif  value="pengguna-update">
                                                    <label class="form-check-label" for="check1">Update</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-check-input"  name="acl[]" @if($isView) disabled {{$acl->contains('access','pengguna-delete') ? 'checked':''}} @elseif($isUpdate) {{$acl->contains('access','pengguna-delete') ? 'checked':''}} @endif  value="pengguna-delete">
                                                    <label class="form-check-label" for="check1">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($isView == false)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
