{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Kelola Pengguna</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Kelola Pengguna</a></li>
                    </ol>
                </div>
            </div>
            @if(checkACL("pengguna-add"))
            <div>
                <a href="{{route('userTambah')}}" type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i>Tambah Pengguna</a>
            </div>
            @endif
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example8" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$x++}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->nama}}</td>
                                    <td>
                                        <div class="btn-group ">
                                            @if(checkACL("pengguna-view"))<a type="button" href="{{route('userView',$user->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-eye mdi-18px"></i></a>@endif
                                            @if(checkACL("pengguna-update"))<a type="button" href="{{route('userUpdate',$user->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                            @if(checkACL("pengguna-delete"))<a type="button" href="{{route('userDelete',$user->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
