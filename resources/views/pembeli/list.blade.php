{{-- Extends layout --}}
@extends('layout.default')



{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Data Pembeli</a></li>
                    </ol>
                </div>
            </div>
            @if(checkACL("pembeli-pembelian-add"))
                <div>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="mdi mdi-plus"></i> Tambah Data Pembeli</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('tambahPembeliKavling')}}">Kavling</a>
                        <a class="dropdown-item" href="{{route('tambahPembeliBangunanCash')}}">Bangunan (Cash)</a>
                        <a class="dropdown-item" href="{{route('tambahPembeliBangunanKredit')}}">Bangunan (Kredit)</a>
                    </div>
                </div>
            @endif
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example3" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pembeli</th>
                                    <th>No HP</th>
                                    <th>Tipe</th>
                                    <th>Lokasi Rumah</th>
                                    <th>Pembayaran</th>
                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($pembelian as $pembeli)
                                    <tr>
                                        <td>{{$x++}}</td>
                                        <td>{{$pembeli->nama_pembeli}}</td>
                                        <td>{{$pembeli->nomor_handphone}}</td>
                                        @if($pembeli->jenis == "kavling")
                                            <td>Kavling</td>
                                        @else
                                            <td>Barokah {{$pembeli->luas_bangunan}}/{{$pembeli->luas_tanah}}</td>
                                        @endif
                                        <td>{{$pembeli->alamatpembelian->alamat}}</td>
                                        <td>{{$pembeli->dp == 0 ? 'Cash':'Cicilan '.$pembeli->pilihanpembayaran_id.' Tahun'}}</td>
                                        <td>
                                            <div class="btn-group ">
                                                @if(checkACL("pembeli-pembelian-view"))<a type="button" href="{{route('viewPembeli',$pembeli->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-eye mdi-18px"></i></a>@endif
                                                @if(checkACL("pembeli-pembelian-update"))<a type="button" href="{{route('updatePembeli',$pembeli->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                                @if(checkACL("pembeli-pembelian-delete"))<a type="button" href="{{route('pembelianDelete',$pembeli->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
