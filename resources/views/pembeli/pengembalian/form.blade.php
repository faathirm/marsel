{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate == true)
                    <h2 class="text-black font-w600">Update Data Pengembalian</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Pengembalian</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$pengembalians->id}}</a></li>
                    </ol>
                @elseif($isView)
                    <h2 class="text-black font-w600">Lihat Data Pengembalian</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Pengembalian</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$pengembalians->id}}</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Tambah Data Pengembalian</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Pengembalian</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah Data Pengembalian Baru</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="post" @if($isUpdate == true) action="{{route('pengembalianUpdatePost')}}" @else action="{{route('pengembalianNewPost')}}" @endif>
                                @csrf
                                @if($isUpdate == true) <input type="hidden" name="id" value="{{$pengembalians->id}}"> @endif
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pembeli</label>
                                    <div class="col-sm-9">
                                        <select class="form-control pengembalianPembeli {{$isView?'bg-light':""}}" {{$isView?'disabled':""}} name="pembelian_id">
                                            @foreach($pembelians as $pembelian)
                                                <option @if($isView && $pengembalians->pembelian_id == $pembelian->id) selected @elseif($isUpdate && $pengembalians->pembelian_id == $pembelian->id) selected @endif value="{{$pembelian->id}}">{{$pembelian->nama_pembeli}} - {{$pembelian->alamatpembelian->alamat}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pengembalianAlamat" name="alamat" @if($isView || $isUpdate) value="{{$pengembalians->pembelian->alamatpembelian->alamat}}" @endif  readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Akad</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pengembalianTanggalAkad" name="tanggal_akad" @if($isView || $isUpdate) value="{{$pengembalians->pembelian->tanggal_akad}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pengembalianTanggalPembayaran" name="tanggal_pembayaran" @if($isView || $isUpdate) value="{{$pengembalians->pembelian->tanggal_pembayaran}}" @endif  readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Pembayaran</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pengembalianTotalPembayaran" name="total_pembayaran" @if($isView || $isUpdate) value="Rp. {{number_format($totalyangsudahdibayarkan)}}" @endif  readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <table class="table table-bordered col-sm-12" id="pengembalianTable">
                                        <thead>
                                            <tr>
                                                <td>Tanggal Pengembalian</td>
                                                <td>Jumlah Pengembalian</td>
                                                <td class="text-center p-1"><button type="button" class="btn text-primary btn-sm m-1 pengembalianAdd" {{$isView?'disabled':""}}><i class="mdi mdi-plus"></i></button></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($isView || $isUpdate)
                                                @foreach($daftarpengembalians as $dp)
                                                    <tr>
                                                        <td><input type="text" class="form-control pengembalianTanggalPembayaran {{$isView?'bg-light':""}}" {{$isView?'readonly':""}} name="tanggalPengembalian[]" value="{{$dp->tanggal}}"></td>
                                                        <td><input type="text" class="form-control pengembalianJumlah {{$isView?'bg-light':""}}" {{$isView?'readonly':""}} name="jumlahPengembalian[]" value="Rp. {{number_format($dp->jumlah)}}"></td>
                                                        <td class="text-center p-1"><button type="button" class="btn text-danger btn-sm m-1 pengembalianRemove" {{$isView?'disabled':""}}><i class="mdi mdi-minus"></i></button></td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Pengembalian</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pengembalianTotal" name="total_pengembalian" @if($isView || $isUpdate) value="Rp. {{number_format($totalpengembalian)}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Sisa Uang</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pengembalianSisaUang" name="sisa_uang" @if($isView || $isUpdate) value="Rp. {{number_format($sisa)}}" @endif readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Keterangan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control {{$isView?'bg-light':""}}" {{$isView?'readonly':""}} name="keterangan">@if($isView || $isUpdate){{$pengembalians->keterangan}}@endif</textarea>
                                    </div>
                                </div>
                                @if(!$isView)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
