{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="form-head page-titles d-flex  align-items-center">
                <div class="mr-auto  d-lg-block">
                    <h2 class="text-black font-w600">Pengembalian</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Data Pengembalian</a></li>
                    </ol>
                </div>
            </div>
            @if(checkACL('pembeli-pengembalian-add'))
                <div>
                    <a href="{{route('pengembalianNew')}}" type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i> Tambah Data Pengembalian</a>
                </div>
            @endif
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example9" class="display min-w850 text-center">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pembeli</th>
                                    <th>Lokasi Rumah</th>
                                    <th>Total Pengembalian</th>
                                    <th width="5"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $x=1; @endphp
                                @foreach($pengembalians as $pengembalian)
                                @php $dp = \App\Models\Daftarpengembalian::where('pengembalian_id',$pengembalian->id)->sum('jumlah'); @endphp
                                    <tr>
                                        <td>{{$x++}}</td>
                                        <td>{{$pengembalian->pembelian->nama_pembeli}}</td>
                                        <td>{{$pengembalian->pembelian->alamatpembelian->alamat}}</td>
                                        <td>Rp. {{number_format($dp)}}</td>
                                        <td>
                                            <div class="btn-group ">
                                                @if(checkACL('pembeli-pengembalian-view'))<a type="button" href="{{route('pengembalianView',$pengembalian->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-eye mdi-18px"></i></a>@endif
                                                @if(checkACL('pembeli-pengembalian-update'))<a type="button" href="{{route('pengembalianUpdate',$pengembalian->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-pencil mdi-18px"></i></a>@endif
                                                @if(checkACL('pembeli-pengembalian-delete'))<a type="button" href="{{route('pengembalianDelete',$pengembalian->id)}}" class="btn btn-outline-dark"><i class="mdi mdi-delete mdi-18px"></i></a>@endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
