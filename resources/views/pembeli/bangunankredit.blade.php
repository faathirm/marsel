{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate == true)
                    <h2 class="text-black font-w600">Update Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Pembeli</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$pembeli->id}}</a></li>
                    </ol>
                @elseif($isView == true)
                    <h2 class="text-black font-w600">View Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Pembeli</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$pembeli->id}}</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Data Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Pembeli</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah Data Pembeli Baru</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <form method="POST" @if($isUpdate) action="{{route('pembeliUpdateBangunanKreditPost')}}" @else action="{{route('pembeliBangunanKreditNewPost')}}" @endif enctype="multipart/form-data">
            @csrf
            @if($isUpdate) <input type="hidden" name="id" value="{{$pembeli->id}}"> @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Data Pembeli
                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pembeli</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="nama_pembeli" @if($isView || $isUpdate) value="{{$pembeli->nama_pembeli}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">NIK</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="nik" @if($isView || $isUpdate) value="{{$pembeli->nik}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="alamat" @if($isView || $isUpdate) value="{{$pembeli->alamat}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No HP</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="no_handphone" @if($isView || $isUpdate) value="{{$pembeli->nomor_handphone}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Berkas</label>
                                    <div class="col-sm-9">
                                        <div class="table-responsive">
                                            <table class="display table-bordered w-100">
                                                <tr>
                                                    <td class="w-25 text-center">KTP</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/ktp/'.$pembeli->ktp)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                        <td>
                                                            <div class="custom-file">
                                                                <input name="ktp" type="file" class="custom-file-input">
                                                                <label class="custom-file-label">Choose file</label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="text-center">NPWP</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/npwp/'.$pembeli->npwp)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                        <td>
                                                            <div class="custom-file">
                                                                <input name="npwp" type="file" class="custom-file-input">
                                                                <label class="custom-file-label">Choose file</label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Surat Nikah</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/suratnikah/'.$pembeli->surat_nikah)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                        <td>
                                                            <div class="custom-file">
                                                                <input name="surat_nikah" type="file" class="custom-file-input">
                                                                <label class="custom-file-label">Choose file</label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Surat Akta</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/suratakad/'.$pembeli->surat_akta)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                        <td>
                                                            <div class="custom-file">
                                                                <input name="surat_akad" type="file" class="custom-file-input">
                                                                <label class="custom-file-label">Choose file</label>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    @php
                    if($isView || $isUpdate){
                        $hargaCash = ($pembeli->luas_tanah*getParameter('harga_tanah'))+($pembeli->luas_bangunan * $pembeli->harga_meter);
                        $nilaiKredit = $hargaCash-$pembeli->dp;
                        $jumlahCicilan = ($nilaiKredit/($pembeli->pilihanpembayaran_id*12))+($nilaiKredit*(0.08/12));
                        $jumlahKreditPokok = $jumlahCicilan*($pembeli->pilihanpembayaran_id*12);
                    }

                    @endphp
                    <div class="card pembeliCardRumah">
                        <div class="card-header">
                            Data Pembelian Rumah
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="alamat_id" class="alamat_id" @if($isUpdate == true) value="{{$pembeli->alamat_id}}" @endif>
                            <div class="basic-form">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tipe Rumah</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control pembeliLuasBangunan {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="luas_bangunan_rumah" placeholder="Luas Bangunan" @if($isView || $isUpdate) value="{{$pembeli->luas_bangunan}}" @endif>
                                        <input type="text" class="form-control pembeliLuasTanah {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="luas_tanah_rumah" placeholder="Luas Tanah" @if($isView || $isUpdate) value="{{$pembeli->luas_tanah}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan Pembayaran</label>
                                    <div class="col-sm-9">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-white border-light">Cicilan</span>
                                            </div>
                                            <input type="text" class="form-control pembeliPembayaran {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="pilihanpembayaran_id_rumah" @if($isView || $isUpdate) value="{{$pembeli->pilihanpembayaran_id}}" @endif>
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-white border-light">Tahun</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Harga Rumah per Meter</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control pembeliHargaRumahM {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="harga_meter_rumah" @if($isView || $isUpdate) value="{{"Rp. ".number_format($pembeli->harga_meter)}}" @else value="Rp. 0" @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Total Harga Cash</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control bg-light pembeliTotalHargaRumah" data-value="{{getParameter("harga_tanah")}}" @if($isView || $isUpdate) value="{{"Rp. ".number_format($hargaCash)}}" @else value="Rp. 0" @endif name="harga_rumah" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">DP</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pembeliHargaDp {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="dp_rumah" @if($isView || $isUpdate) value="{{"Rp. ".number_format($pembeli->dp)}}" @else value="Rp. 0" @endif>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Nilai Kredit</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pembeliNilaiKredit bg-light" @if($isView || $isUpdate) value="{{"Rp. ".number_format($nilaiKredit)}}" @else value="Rp. 0" @endif name="nilai_kredit_rumah" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Jumlah Cicilan Tiap Bulan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pembeliJumlahCicilan bg-light" @if($isView || $isUpdate) value="{{"Rp. ".number_format($jumlahCicilan)}}" @else value="Rp. 0" @endif name="jumlah_cicilan_rumah" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Jumlah Kredit Pokok</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pembeliJumlahKreditPokok bg-light" @if($isView || $isUpdate) value="{{"Rp. ".number_format($jumlahKreditPokok)}}" @else value="Rp. 0" @endif name="jumlah_kredit_rumah" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Total Harga Kredit</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control pembeliTotalHargaRumahKredit bg-light" @if($isView || $isUpdate) value="{{"Rp. ".number_format($pembeli->harga)}}" @else value="Rp. 0" @endif name="total_kredit_rumah" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Lokasi Rumah</label>
                                <div class="col-sm-9">
                                    <button type="button" class="btn btn-block text-left buttonDropdownAlamat dropdown-toggle {{$isView?"bg-light":"btn-outline-light"}}" @if($isView == false)data-toggle="dropdown"@endif>
                                        @if($isUpdate || $isView) {{$pembeli->alamatpembelian->alamat}} @else Pilih Alamat @endif
                                    </button>
                                    <div class="dropdown-menu w-75">
                                        <input data-value="*" type="text" class="border-light form-control searchAlamatKeluar"  placeholder="Cari alamat...">
                                        @foreach($alamats  as $alamat)
                                            <a class="dropdown-item dropdownAlamat" data-value="{{$alamat->id}}" href="#" >{{$alamat->alamat}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Tanggal Pembayaran</label>
                                <div class="col-sm-9">
                                    <input name="tanggal_pembayaran_rumah" class="form-control {{$isView?"bg-light":"datepicker-default "}}" {{$isView?"readonly":null}} @if($isView || $isUpdate) value="{{$pembeli->tanggal_pembayaran}}" @endif id="datepicker">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Tanggal Akad</label>
                                <div class="col-sm-9">
                                    <input name="tanggal_akad_rumah" class="form-control {{$isView?"bg-light":"datepicker-default "}}" {{$isView?"readonly":null}} @if($isView || $isUpdate) value="{{$pembeli->tanggal_akad}}" @endif id="datepicker">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Jenis Pembayaran</label>
                                <div class="col-sm-9">
                                    <select name="jenis_pembayaran_rumah" {{$isView?"disabled":null}} class="form-control {{$isView?"bg-light":null}}">
                                        <option value="Tunai" @if($isView && $pembeli->jenis_pembayaran == "Tunai") Selected @elseif($isUpdate && $pembeli->jenis_pembayaran == "Tunai") Selected @endif>Tunai</option>
                                        <option value="Transfer" @if($isView && $pembeli->jenis_pembayaran == "Transfer") Selected @elseif($isUpdate && $pembeli->jenis_pembayaran == "Transfer") Selected @endif>Transfer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">No Kwitansi</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="no_kwitansi_rumah" @if($isView || $isUpdate) value="{{$pembeli->no_kwitansi}}" @endif>
                                </div>
                            </div>
                            @if($isView==false)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>

@endsection
