{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="form-head page-titles d-flex  align-items-center">
            <div class="mr-auto  d-lg-block">
                @if($isUpdate)
                    <h2 class="text-black font-w600">Update Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Pembeli</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$pembeli->id}}</a></li>
                    </ol>
                @elseif($isView)
                    <h2 class="text-black font-w600">View Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Pembeli</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">#{{$pembeli->id}}</a></li>
                    </ol>
                @else
                    <h2 class="text-black font-w600">Data Pembeli</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Data Pembeli</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah Data Pembeli Baru</a></li>
                    </ol>
                @endif
            </div>
        </div>
        <!-- row -->
        <form method="POST" @if($isUpdate) action="{{route('pembeliUpdatePost')}}" @else action="{{route('pembeliNewPost')}}" @endif enctype="multipart/form-data">
            @csrf
            @if($isUpdate) <input type="hidden" name="id" value="{{$pembeli->id}}"> @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Data Pembeli
                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pembeli</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="nama_pembeli" @if($isView || $isUpdate) value="{{$pembeli->nama_pembeli}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">NIK</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="nik" @if($isView || $isUpdate) value="{{$pembeli->nik}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="alamat" @if($isView || $isUpdate) value="{{$pembeli->alamat}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No HP</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="no_handphone" @if($isView || $isUpdate) value="{{$pembeli->nomor_handphone}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Berkas</label>
                                    <div class="col-sm-9">
                                        <div class="table-responsive">
                                            <table class="display table-bordered w-100">
                                                <tr>
                                                    <td class="w-25 text-center">KTP</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/ktp/'.$pembeli->ktp)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                    <td>
                                                        <div class="custom-file">
                                                            <input name="ktp" type="file" class="custom-file-input">
                                                            <label class="custom-file-label">Choose file</label>
                                                        </div>
                                                    </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="text-center">NPWP</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/npwp/'.$pembeli->npwp)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                    <td>
                                                        <div class="custom-file">
                                                            <input name="npwp" type="file" class="custom-file-input">
                                                            <label class="custom-file-label">Choose file</label>
                                                        </div>
                                                    </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Surat Nikah</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/suratnikah/'.$pembeli->surat_nikah)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                    <td>
                                                        <div class="custom-file">
                                                            <input name="surat_nikah" type="file" class="custom-file-input">
                                                            <label class="custom-file-label">Choose file</label>
                                                        </div>
                                                    </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Surat Akta</td>
                                                    @if($isView || $isUpdate) <td class="text-center"><a class="text-primary" href="{{asset('/doc/suratakad/'.$pembeli->surat_akta)}}">View</a></td> @endif
                                                    @if($isView == false)
                                                    <td>
                                                        <div class="custom-file">
                                                            <input name="surat_akad" type="file" class="custom-file-input">
                                                            <label class="custom-file-label">Choose file</label>
                                                        </div>
                                                    </td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card pembeliCardKavling">
                        <div class="card-header">
                            Data Pembelian Kavling
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="alamat_id" class="alamat_id" @if($isUpdate) value="{{$pembeli->alamat_id}}" @endif>
                            <div class="basic-form">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tipe Kavling</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control pembeliLuasTanah {{$isView?"bg-light":null}}" {{$isView?"readonly":null}} name="luas_tanah_kavling" @if($isView || $isUpdate) value="{{$pembeli->luas_tanah}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pembayaran</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light {{$isView?"bg-light":null}}" value="Cash" readonly>
                                        <input type="hidden" name="pilihanpembayaran_id_kavling" value="1">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Harga Kavling</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control bg-light pembeliHargaKavling {{$isView?"bg-light":null}}" data-value="{{getParameter("harga_tanah")}}" name="harga_kavling" @if($isView || $isUpdate)value="{{"Rp. ".number_format($pembeli->harga)}}"@endif readonly>
                                        <input type="hidden" name="dp_kavling" value="0">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Lokasi Kavling</label>
                                    <div class="col-sm-9">
                                        <button type="button" class="btn btn-block text-left buttonDropdownAlamat dropdown-toggle {{$isView?"bg-light":"btn-outline-light"}}" @if($isView == false)data-toggle="dropdown"@endif>
                                            @if($isUpdate || $isView) {{$pembeli->alamatpembelian->alamat}} @else Pilih Alamat @endif
                                        </button>
                                        <div class="dropdown-menu w-75">
                                            <input data-value="*" type="text" class="border-light form-control searchAlamatKeluar"  placeholder="Cari alamat...">
                                            @foreach($alamats  as $alamat)
                                                <a class="dropdown-item dropdownAlamat" data-value="{{$alamat->id}}" href="#" >{{$alamat->alamat}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Pembayaran</label>
                                    <div class="col-sm-9">
                                        <input name="tanggal_pembayaran_kavling" class="form-control {{$isView?"bg-light":"datepicker-default "}}" {{$isView?"disabled":null}} id="datepicker" @if($isView || $isUpdate) value="{{$pembeli->tanggal_pembayaran}}" @endif >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Akad</label>
                                    <div class="col-sm-9">
                                        <input name="tanggal_akad_kavling" class="form-control {{$isView?"bg-light":"datepicker-default "}}" {{$isView?"disabled":null}} @if($isView || $isUpdate) value="{{$pembeli->tanggal_akad}}" @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis Pembayaran</label>
                                    <div class="col-sm-9">
                                        <select name="jenis_pembayaran_kavling" {{$isView?"disabled":null}} class="form-control {{$isView?"bg-light":null}}">
                                            <option value="Tunai" {{$isView || $isUpdate && $pembeli->jenis_pembayaran == "Tunai" ? "Selected" :null}}>Tunai</option>
                                            <option value="Transfer" {{$isView || $isUpdate && $pembeli->jenis_pembayaran == "Transfer" ? "Selected" :null}}>Transfer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No Kwitansi</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control {{$isView?"bg-light":null}}" name="no_kwitansi_kavling" {{$isView?"readonly":null}} @if($isView || $isUpdate) value="{{$pembeli->no_kwitansi}}" @endif>
                                    </div>
                                </div>
                                @if($isView == false)
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>

@endsection
