

(function($) {
    "use strict"
    //example 1
    var table = $('#example').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        columnDefs:[{
          'targets':[7],
           'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
        footerCallback: function ( tfoot, data, start, end, display){
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[^0-9]+/g, "")*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total Kuantitas
            var totalKuantitas = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 4 ).footer() ).html(
                numberWithCommas(totalKuantitas)
            );

            // Harga Satuan
            var hargaSatuan = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 5 ).footer() ).html(
                'Rp '+numberWithCommas(hargaSatuan)
            );

            // Total Pengeluaran
            var totalPengeluaran = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 6 ).footer() ).html(
                'Rp '+numberWithCommas(totalPengeluaran)
            );

            // Grand Total
            var grandTotal = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $(".grandTotal").html('Rp '+numberWithCommas(grandTotal));
        }
    });

    var table = $('#example2').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        columnDefs:[{
            'targets':[10],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
        footerCallback: function ( tfoot, data, start, end, display){
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[^0-9]+/g, "")*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total Kuantitas Barang
            var totalKuantitasBarang = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 7 ).footer() ).html(
                numberWithCommas(totalKuantitasBarang)
            );

            // Total Kuantitas
            var totalKuantitas = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 8 ).footer() ).html(
                'Rp '+numberWithCommas(totalKuantitas)
            );


            // Total Pengeluaran
            var totalPengeluaran = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 9 ).footer() ).html(
                'Rp '+numberWithCommas(totalPengeluaran)
            );

            // Grand Total
            var grandTotal = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $(".grandTotal").html('Rp '+numberWithCommas(grandTotal));
        }
    });

    var table = $('#example3').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        columnDefs:[{
            'targets':[6],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
    });

    var table = $('#example4').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        createdRow: function ( row, data, index ) {
        },
    });

    //pembayaran
    var table = $('#example5').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        pageLength:5,
        lengthMenu: [5, 10, 25],
        columnDefs:[{
            'targets':[5],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
        footerCallback: function ( tfoot, data, start, end, display){
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[^0-9]+/g, "")*1 :
                    typeof i === 'number' ?
                        i : 0;
            };


            // Total Pengeluaran
            var totalPengeluaran = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 4 ).footer() ).html(
                'Rp '+numberWithCommas(totalPengeluaran)
            );

            // Grand Total
            var grandTotal = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $(".grandTotalAir").html('Rp '+numberWithCommas(grandTotal));
        }
    });

    var table = $('#example6').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        pageLength:5,
        lengthMenu: [5, 10, 25],
        columnDefs:[{
            'targets':[5],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
        footerCallback: function ( tfoot, data, start, end, display){
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[^0-9]+/g, "")*1 :
                    typeof i === 'number' ?
                        i : 0;
            };


            // Total Pengeluaran
            var totalPengeluaran = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 4 ).footer() ).html(
                'Rp '+numberWithCommas(totalPengeluaran)
            );

            // Grand Total
            var grandTotal = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $(".grandTotalCicilan").html('Rp '+numberWithCommas(grandTotal));
        }
    });

    var table = $('#example7').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        columnDefs:[{
            'targets':[5],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
        footerCallback: function ( tfoot, data, start, end, display){
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[^0-9]+/g, "")*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

        }
    });

    //USER
    var table = $('#example8').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        columnDefs:[{
            'targets':[3],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
        footerCallback: function ( tfoot, data, start, end, display){
            var api = this.api();

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[^0-9]+/g, "")*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

        }
    });

    var table = $('#example9').DataTable({
        language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.25/i18n/Indonesian.json"
        },
        columnDefs:[{
            'targets':[4],
            'orderable':false
        }],
        createdRow: function ( row, data, index ) {
        },
    });


    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

})(jQuery);
