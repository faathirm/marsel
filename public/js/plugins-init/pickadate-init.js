(function($) {
    "use strict"

    //date picker classic default
    $('.datepicker-default').pickadate({
        monthsFull:['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
        format: 'dd/mm/yyyy'
    });

    $(document).on("click",'.datepicker-default1',function () {
        $(this).pickadate({
            monthsFull:['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
            format: 'dd/mm/yyyy'
        });
    });


})(jQuery);
