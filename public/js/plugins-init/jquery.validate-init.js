jQuery(".form-valide").validate({
    rules: {
        "val-username": {
            required: !0,
            minlength: 3
        },
        "val-email": {
            required: !0,
            email: !0
        },
        "val-password": {
            required: !0,
            minlength: 5
        },
        "val-confirm-password": {
            required: !0,
            equalTo: "#val-password"
        },
        "val-select2": {
            required: !0
        },
        "val-select2-multiple": {
            required: !0,
            minlength: 2
        },
        "val-suggestions": {
            required: !0,
            minlength: 5
        },
        "val-skill": {
            required: !0
        },
        "val-currency": {
            required: !0,
            currency: ["$", !0]
        },
        "val-website": {
            required: !0,
            url: !0
        },
        "val-phoneus": {
            required: !0,
            phoneUS: !0
        },
        "val-digits": {
            required: !0,
            digits: !0
        },
        "val-number": {
            required: !0,
            number: !0
        },
        "val-range": {
            required: !0,
            range: [1, 5]
        },
        "val-terms": {
            required: !0
        }
    },
    messages: {
        "val-username": {
            required: "Please enter a username",
            minlength: "Your username must consist of at least 3 characters"
        },
        "val-email": "Please enter a valid email address",
        "val-password": {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long"
        },
        "val-confirm-password": {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long",
            equalTo: "Please enter the same password as above"
        },
        "val-select2": "Please select a value!",
        "val-select2-multiple": "Please select at least 2 values!",
        "val-suggestions": "What can we do to become better?",
        "val-skill": "Please select a skill!",
        "val-currency": "Please enter a price!",
        "val-website": "Please enter your website!",
        "val-phoneus": "Please enter a US phone!",
        "val-digits": "Please enter only digits!",
        "val-number": "Please enter a number!",
        "val-range": "Please enter a number between 1 and 5!",
        "val-terms": "You must agree to the service terms!"
    },

    ignore: [],
    errorClass: "invalid-feedback animated fadeInUp",
    errorElement: "div",
    errorPlacement: function(e, a) {
        jQuery(a).parents(".form-group > div").append(e)
    },
    highlight: function(e) {
        jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
    },
    success: function(e) {
        jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
    },
});

var kuantitas=0;
var hargasatuan=0;
var hargapembelian=0;


$('.inputRupiah').keyup(function (event) {
    if(event.which >= 37 && event.which <= 40) return;
    $(this).val(function(index, value) {
        hargasatuan = value.replace(/\D/g,"");
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
    });
    console.log("harga satuan: "+hargasatuan);
    hargapembelian = parseInt(kuantitas)*parseInt(hargasatuan);
    if(kuantitas != null && hargasatuan!= null){
        $('.totalPembelian').html('Rp '+numberWithCommas(hargapembelian));
    }
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

//BARANG KELUAR

//INIT
var hargaBarangKeluar = $(".barangKeluar").val().match(/\((.*)\)/).pop().replace(/\D/g, "");
var kuantitasBarangKeluar = $(".kuantitasBarangKeluar").val();
var totalPengeluaran = hargaBarangKeluar*kuantitasBarangKeluar;

console.log("Harga : "+hargaBarangKeluar);
console.log("Kuantitas : "+kuantitasBarangKeluar);
$(".totalPengeluaran").html("Rp "+numberWithCommas(totalPengeluaran));

$(".barangKeluar").on("change", function () {
    var getPrice = $(this).val().match(/\((.*)\)/).pop();

    hargaBarangKeluar = $(".barangKeluar").children("option:selected").val().match(/\((.*)\)/).pop().replace(/\D/g, "");
    kuantitasBarangKeluar = $(".kuantitasBarangKeluar").val();
    totalPengeluaran = hargaBarangKeluar*kuantitasBarangKeluar;

    if(hargaBarangKeluar != null && kuantitasBarangKeluar != null){
        $('.totalPengeluaran').html('Rp '+numberWithCommas(totalPengeluaran));
    }
});

$('.kuantitasBarangKeluar').keyup(function (event) {
    if(event.which >= 37 && event.which <= 40) return;
    $(this).val(function(index, value) {
        kuantitas = value.replace(/\D/g,"");
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
    });

    hargaBarangKeluar = $(".barangKeluar").children("option:selected").val().match(/\((.*)\)/).pop().replace(/\D/g, "");
    kuantitasBarangKeluar = $(".kuantitasBarangKeluar").val();
    totalPengeluaran = hargaBarangKeluar*kuantitasBarangKeluar;

    if(hargaBarangKeluar != null && kuantitasBarangKeluar != null){
        $('.totalPengeluaran').html('Rp '+numberWithCommas(totalPengeluaran));
    }
});



jQuery(".form-valide-with-icon").validate({
    rules: {
        "val-username": {
            required: !0,
            minlength: 3
        },
        "val-password": {
            required: !0,
            minlength: 5
        }
    },
    messages: {
        "val-username": {
            required: "Please enter a username",
            minlength: "Your username must consist of at least 3 characters"
        },
        "val-password": {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long"
        }
    },

    ignore: [],
    errorClass: "invalid-feedback animated fadeInUp",
    errorElement: "div",
    errorPlacement: function(e, a) {
        jQuery(a).parents(".form-group > div").append(e)
    },
    highlight: function(e) {
        jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
    },
    success: function(e) {
        jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-valid")
    }




});
